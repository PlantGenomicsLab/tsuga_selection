Code and records for the hemlock terpenoid selection project. Posted here are the code and analyses to generate a reference transcriptome for both the eastern hemlock (*Tsuga canadensis*) and the Chinese hemlock (*Tsuga chinensis*), EnTAP annotation, transcriptome homology, and population genomic analyses. The goal of the analysis is to understand patterns of selection on terpenoid synthase genes. 

[TOC]

## Sequence information

Input data for this study are pairs of RNAseq libraries sequenced at the UGA Core Facilities (Project \#3930), libraries downloaded from the SRA, and libraries sequenced by NovoGene for Vidya Vuruputoor's transcriptome project. 

| | Fetter | Swensen | Feng | Vuruputoor | Total Read Sets |
|:--- | ---:| ---:| ---:| ---:|  ---:|
| canadensis | 8 | 5 | 4 | 3 | 20 |
| chinensis | 8 | 0 | 12 | 1 | 21 |
| SRA Project | [PRJNA946770](https://www.ncbi.nlm.nih.gov/search/all/?term=PRJNA946770) | [PRJNA413418](https://www.ncbi.nlm.nih.gov/search/all/?term=PRJNA413418) | [PRJNA650252](https://www.ncbi.nlm.nih.gov/search/all/?term=PRJNA650252) | TBD | |

> RNA resources used in this study. The First authors of each dataset are listed in the first row, followed by counts of read sets by species. The project for Fetter's reads are currently embargoed. Detailed data on each read pair is avaialable [here](metadata/read_set_info.txt).


```
git clone https://github.com/CBC-UCONN/RNAseq_nonmodel.git
```

Rename `RNAseq_nonmodel` to `assembly`. Gather the libraries into `assembly/01_raw_reads` for *canadensis* and *chinensis*, respectively. I already had the \#3930 and Vidya's reads on my HPC and moved them over. The Reads on the SRA were downloaded with [trinityfetchSRA_sp.sh](scripts/trinityfetchSRA_sp.sh). Use the appropriate bash file for each dir to download the sequences. Then move \#3930 and Vidya's sequences to the project dir.

Currently (Nov22) `/core/labs/Wegrzyn` is low on space and the raw reads are very large and have been moved to `/archive/labs/wegrzyn/transcriptomes/conifers/hemlock/kfetter`. Symlink the reads to `data/assembly/01_Raw_Reads` using [raw_data_symlinks.sh](scripts/raw_data_symlinks.sh). Remeber to provide the specific path for the reads from each species in the script. Currently, the following read sets are in the project, and stored at `/archive/labs/wegrzyn/transcriptomes/conifers/hemlock/kfetter`. 


## Submitting reads to the SRA

The process deserves a git in its own, but for now. I'll have some scratch records here.

1. Register with the SRA
2. Upload the metadata for the accessions and the read files
3. Get all of the files into a single `tar.gz` file
4. Use [ftp.sh](scripts/ftp.sh) to transfer the files to the SRA
5. Submit the preloaded file using the SRA submission guide (linked below).
6. Record the project number and SRR-id's for each sample in a file and send to supplemental materials.

Here's the link to finish off the SRA submission: https://submit.ncbi.nlm.nih.gov/subs/sra/SUB12947612/files 

As a note, the method I used to succesfully transfer the reads was the following:

1. Open a terminal locally
2. Start a screen
3. ssh into transfer node
4. cd to dir containing sra_upload.tar.gz
5. ftp into SRA's site
6. cd to my upload dir
7. Make a new folder called `tsuga_rna_ftp1`
8. `put sra_upload.tar.gz`
9. After about 20 minutes it transferred.
10. `bye`

This worked for the 97.1GB sized file. Larger files may not work with this method, and using `ftp.sh` is better.

_scratch_

```
cd  /archive/labs/wegrzyn/transcriptomes/conifers/hemlock/kfetter/tsuga_chinensis
```
Verify all the files are there
Look in tarSRA_6048857.* to see this list of files.
Everything is there!
Follow the FTP upload instructions
 
1. Start an interactive session
2. Log into the transfer node `ssh kfetter@transfer.cam.uchc.edu`
3. Go to the dir where the `tar.gz` is kept
4. Start an ftp session to ncbi: `ftp ftp-private.ncbi.nlm.nih.gov`
   1. Username: subftp, Pass: 1CVwWYg7
5. Navigate to my dir: `cd uploads/karl.fetter_gmail.com_1631vtKH`
6. Make a new folder: `mkdir tsuga_rna_ftp`
7. Transfer the file: `put sra_upload.tar.gz`
8. Close the connection: `bye`
9. Return to the sra page and finish it.

If for some reason the interactieve session dies and kills the transfer, try this:
1. On your computer, open up a terminal and start a screen
2. log into to the transfer node `ssh kfetter@transfer.cam.uchc.edu`
3. cd to the dir with the `tar.gz`
4. Start an ftp session, and continue the transfer as above.

## Assembling the Reference Transcriptomes

To follow these analyses, create the directory structure ([dirs.sh](scripts/dirs.sh)) and gather the RNA resources. I'll be using Noah Reid's pipeline for transcriptome assembly from the [RNAseq_nonmodel github](https://github.com/CBC-UCONN/RNAseq_nonmodel). Clone the github repo into the `data/chinensis` and `data/canadensis` dirs. Then follows the steps of the transcriptome assembly pipeline.

### fastQC/multiQC

To perform quality control of the sequenced reads, fastQC and multiQC were run on the reads for each species with the  [`fast-multi_qc.sh`](scripts/fast-multi_qc.sh). 

The percent of duplicate reads in each species differed, which *chinensis* having more percent of duplicated reads (mean = 63.93, SE = 1.128, N = 42) than *canadensis* (mean = 61.28 SE = 1.702, N = 40). Sequencing projects had considerable more variation than species.

*Table of mean percent duplicates by project.*
| Feng | Fetter | Swensen | Vuruputoor |
|:---| :--- |  :--- | :--- |
| 57.97 | 67.75 | 49.67 | 77.03 |

Considerable variation of duplicated reads was observed between accessions, experiments, and species.

![](figs/perc_dups_species_1600x600.jpeg)
*Figure of read uniqueness by species*

![](figs/perc_dups_Project_1050x800.jpeg)

*Figure of read uniqueness by sequencing project.*

### Trimmomatic

Run `Trimmomatic` to remove low quality and residual adapter sequences. This is critical for transcriptome assembly. You can run the [`trimmomatic.sh`](scripts/trimmomatic.sh) script from the `02_Quality_Control` directory. The script will loop over all read pairs in the `filelist`, which you must first make using `ls *fastq.gz > filelist`, and then edit in vim to contain only the unique read pair name (e.g. 'ACH_01'). The adapter library for trimming is borrowed from the ISG. Below is the Trimmomatic command in the loop.

```bash
module load Trimmomatic/0.39

readpairlist=/core/labs/Wegrzyn/karl/tsuga_sel/data/chinensis/assembly/01_Raw_Reads/filelist
for f in `cat $readpairlist`
do
java -jar $Trimmomatic PE -threads 4 \
        ../01_Raw_Reads/${f}_1.fastq.gz \
        ../01_Raw_Reads/${f}_2.fastq.gz \
        trim_${f}_1.fastq.gz singles_trim_${f}_1.fastq.gz \
        trim_${f}_2.fastq.gz singles_trim_${f}_2.fastq.gz \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        SLIDINGWINDOW:4:25 MINLEN:45

done
date
```

## *De novo* read pair assembly with Trinity 

To create assembles from the read pairs, I'll use the pipine [`Trinity`](https://github.com/trinityrnaseq/trinityrnaseq/wiki) (version 2.14.0). Assembly requires a great deal of memory (RAM) and can take few days if the read set is large. The script to run `Trinity` is rather simple, and takes as input the read pairs and few tuning parameters. The only parameter I'll change is the `min_contig_length` and I'll increase it to 350. This should decrease false positives by requiring transcripts to be slightly longer than the default 300 bases. The script to run Trinity on the *canadensis* samples is [here](scripts/trinity.sh). 

   
```bash
module load trinity/2.14.0
module load samtools

# a bash array containing the sample IDs
LIST=($(echo ACN_01 ACN_02 JSP_01 JSP_02 JSP_03 SRR12377238 SRR12377239 SRR12377240 SRR12377241 SRR6134124 SRR6134132 SRR6134201 SRR6134204 SRR6134205 TSP_01 TSP_02 TSP_03 Tca2187_98Aa Tca227_85Ab Tca263_47Aa))

# get one sample ID using the slurm array task ID
SAM=${LIST[$SLURM_ARRAY_TASK_ID]}

Trinity --seqType fq \
  --left ../02_Quality_Control/trim_${SAM}_1.fastq.gz \
  --right ../02_Quality_Control/trim_${SAM}_2.fastq.gz \
  --min_contig_length 350 \
  --CPU 36 \
  --max_memory 200G \
  --output trinity_${SAM} \
  --full_cleanup  
``` 

### Failure of some read pairs

The reads were assembled in the canadensis 350-min_contig_length set for 19 out of 20. ACN_02 failed. I'm not sure why. Check the qc report on that sample. Nothing obvious there. Check the number of reads in the fastq files for each read pair. R1 = 51454775 ; R2 = 51454775. This is good they that have the same number of sequences.

Since I've had some troubles with a few samples dropping out of the trinity pipeline, I'm reapeating the entire process. I submitted the jobs to the himem2 partition, and so far the runs are going well. The *canadensis* sample that didn't assemble has already assembled. I just started the chinensis on the himem (12/20/22). I will return in a few days and evaluate the runs. Both assemblies have run to completion (12/28/22), and they have the correct number of fasta reads. Delete the failed run directories.

## Assembling the reference

The previous assembly step took the trimmed reads and used Trinity's pipeline to assemble reads into a fasta file for each individual. Now, the goal is to create a single reference transcriptome containing a single transcript for each gene. To do this, coding regions of transcripts are identified using `Transdecoder` and `hmmer`. In the next step, redundant transcripts are clustered within and among samples with coding sequences to find a single representative transcript. Finally, amino acid sequences are input to EnTAP for functional annnotation. 
   
### Identify coding regions and protein homology using `TransDecoder` and `HMMER` 

I will use a three step process to identify protein coding regions using a combination of TransDecoder and HMMER. First, using TransDecoder, long open reading frames (ORFs) will be found with the command `TransDecoder.LongOrfs`. `Transdecoder` will find ORFs based on a number of criteria, including maximizing an ORF score (described [here](https://github.com/TransDecoder/TransDecoder/wiki)). The longest ORFs will be input to HMMER's `hmmscan` function to compare the ORFs to a database of known proteins. The database was pre-assembled and located at `/isg/shared/databases/Pfam/Pfam-A.hmm`. Finally, I'll return to TransDecoder and use the prediction function (`TransDecoder.Predict`) using pfam database hits that were found in the `hmmscan`.  

The series of commands are given below and are found in the [`transdecoder.sh`](scripts/transdecoder.sh) script. 

```
module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../03_Assembly/trinity_combine.fasta

hmmscan --cpu 16 \
       --domtblout pfam.domtblout \
       /isg/shared/databases/Pfam/Pfam-A.hmm \
       trinity_combine.fasta.transdecoder_dir/longest_orfs.pep 

TransDecoder.Predict -t ../03_Assembly/trinity_combine.fasta \
	--retain_pfam_hits pfam.domtblout \
	--cpu 16 

```

This will create a series of output files and directories containing the annotations, coding sequences, and tranlsated amino acid sequences of the ORFs, the pfam.datbase and a few other files.


```
-rw-r--r-- 1 kfetter wegrzynlab 434K Dec 23 15:30 CanTransdecoder_5782440.err
-rw-r--r-- 1 kfetter wegrzynlab 5.0G Jan  2 16:49 CanTransdecoder_5782440.out
-rw-r--r-- 1 kfetter wegrzynlab 1.1G Jan  2 16:49 pfam.domtblout
-rw-r--r-- 1 kfetter wegrzynlab  266 Dec 23 12:20 pipeliner.1102126.cmds
-rw-r--r-- 1 kfetter wegrzynlab  266 Dec 23 12:23 pipeliner.3093199.cmds
-rwxr-xr-x 1 kfetter wegrzynlab 1.1K Dec 28 13:24 transdecoder.sh
drwxr-xr-x 2 kfetter wegrzynlab 2.0K Dec 23 13:17 trinity_combine.fasta.transdecoder_dir
drwxr-xr-x 2 kfetter wegrzynlab 1.0K Dec 23 15:30 trinity_combine.fasta.transdecoder_dir.__checkpoints_longorfs

```
Inside the `trinity_combine.fasta.transdecoder_dir` we find the output of the longest ORF search.

```
(base) bash-4.2$ ls -lht trinity_combine.fasta.transdecoder_dir
total 3.1G
-rw-r--r-- 1 kfetter wegrzynlab 1.4G Dec 21 11:25 longest_orfs.gff3
-rw-r--r-- 1 kfetter wegrzynlab 1.3G Dec 21 11:25 longest_orfs.cds
-rw-r--r-- 1 kfetter wegrzynlab 530M Dec 21 11:25 longest_orfs.pep
-rw-r--r-- 1 kfetter wegrzynlab   74 Dec 21 10:21 base_freqs.dat
```

Taking a look at the gff3 file in the *canadensis* assembly, the transcripts are annotated for gene, mRNA, UTRs, and a single exon and CDS sequence.  

*First three annotated transcripts from `longest_orfs.gff3`*
```
ACN_01_TRINITY_DN17285_c0_g1_i1 transdecoder    gene    1       379     .       +       . 
ACN_01_TRINITY_DN17285_c0_g1_i1 transdecoder    mRNA    1       379     .       +       . 
ACN_01_TRINITY_DN17285_c0_g1_i1 transdecoder    exon    1       379     .       +       . 
ACN_01_TRINITY_DN17285_c0_g1_i1 transdecoder    CDS     3       377     .       +       0 

ACN_01_TRINITY_DN17268_c0_g1_i1 transdecoder    gene    1       995     .       -       . 
ACN_01_TRINITY_DN17268_c0_g1_i1 transdecoder    mRNA    1       995     .       -       . 
ACN_01_TRINITY_DN17268_c0_g1_i1 transdecoder    exon    1       995     .       -       . 
ACN_01_TRINITY_DN17268_c0_g1_i1 transdecoder    CDS     194     994     .       -       0 
ACN_01_TRINITY_DN17268_c0_g1_i1 transdecoder    three_prime_UTR 1       193     .       - 

ACN_01_TRINITY_DN17219_c0_g1_i1 transdecoder    gene    1       635     .       -       . 
ACN_01_TRINITY_DN17219_c0_g1_i1 transdecoder    mRNA    1       635     .       -       . 
ACN_01_TRINITY_DN17219_c0_g1_i1 transdecoder    exon    1       635     .       -       . 
ACN_01_TRINITY_DN17219_c0_g1_i1 transdecoder    CDS     135     635     .       -       0 
ACN_01_TRINITY_DN17219_c0_g1_i1 transdecoder    three_prime_UTR 1       134     .       -
```

The primary output of `hmmscan` is `pfam.domtblout`, which contains information on the transcripts, the proteins they match in the database, and some other information.

```
#                                                                                          --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name                         accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- -----               -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
Abhydrolase_3        PF07859.14   211 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125   9.6e-05   22.4   0.9   1   1   2.7e-07   0.00072   19.5   0.9    44   109    56   117    19   123 0.81 alpha/beta hydrolase fold
Lipase_3             PF01764.26   142 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125    0.0025   17.7   0.0   1   1   9.5e-07    0.0025   17.7   0.0    50    87    63   101     9   112 0.89 Lipase (class 3)
Abhydrolase_6        PF12697.8    220 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125    0.0088   16.7   0.2   1   1   3.8e-06    0.0099   16.5   0.2    41    87    46   101    18   117 0.68 Alpha/beta hydrolase family
Hydrolase_4          PF12146.9    239 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125     0.014   14.7   0.0   1   1   6.9e-06     0.018   14.3   0.0    52   102    54   103    45   107 0.83 Serine aminopeptidase, S33
Thioesterase         PF00975.21   230 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125     0.027   14.6   0.0   1   1   1.1e-05     0.028   14.6   0.0    67    94    80   107    24   117 0.84 Thioesterase domain
Peptidase_S9         PF00326.22   212 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125      0.13   11.8   0.0   1   1   6.8e-05      0.18   11.3   0.0    46    87    62   102    53   103 0.90 Prolyl oligopeptidase family
DUF2974              PF11187.9    224 ACN_01_TRINITY_DN17285_c0_g1_i1.p1 -            125      0.18   11.3   0.0   1   1   8.8e-05      0.23   11.0   0.0    77   107    70   102    54   116 0.80 Protein of unknown function (DUF2974)
```

For both the *canadensis* and *chinensis* transcriptomes, the TransDecoder/HMMER pipeline lasted several days. The longest part of the pipeline was the `hmmscan`.


### Running `Transdecoder` and `HMMER`

#### Add unique names to transcripts with `names.sh`

`TransDecoder` will be run on a single combined file of all the assemblies. Due to `Trinity's` naming convention, there are likely to be identical sequence names appearing in each assembly file. These redundant names would confuse `TransDecoder` and turn some of its output into nonsense. To solve the issue, add a sample ID prefix to each sequence name using the linux utility `sed`. Use a list function to set the file names in the bash script, and then use an array job to get each sed command in it's own job. This may not be neccessary, but it will speed up this step. The bash scrip for this job is [`names.sh`](scripts/names.sh)

```bash
# a bash array containing the sample IDs
LIST=($(echo ACH_01 ACH_02 ACH_03 ACH_04 ACH_05 ACH_06 ACH_12 ACH_13 SRR12377231 SRR12377232 SRR12377233 SRR12377234 SRR12377235 SRR12377237 SRR12377248 SRR12377259 SRR12377268 SRR12377269 SRR12377270 SRR12377271 Tch2007_Ba))

# get one sample ID using the slurm array task ID
SAM=${LIST[$SLURM_ARRAY_TASK_ID]}

sed "s/>/>${SAM}_/g" ../03_Assembly/trinity_${SAM}.Trinity.fasta > ../03_Assembly/trinity_prefix_${SAM}.Trinity.fasta
```

To run the command on a single file, remove the `LIST` function and input the name of the file as `SAM=ind_code`.


#### Submitting `transdecoder.sh`

Submit `transdecoder.sh` with `sbatch` and be prepared to wait. The job for *chinensis* was submitted on 12/28/22 and it took about 18 days to run.

### Generate Reference Transcriptome with `vsearch`

Generate a referene transcriptome for each species using the coding sequences identified by `TransDecoder`/`HMMER`. `vsearch` is a tool with many uses, here, I'll use it to take in a set of transcript coding sequences (`trinity_combine.fasta.transdecoder.cds`) and find a single representative transcript sequences. `vsearch` will take input transcripts and cluster them, then output a centroid of each cluster (`centroids.fasta`) representing the reference sequences for that transcript. Ideally, differences in splicing will not effect the outcome, and a reference transcript will be found with potentially many isoforms. Use the [`vsearch.sh`](scripts/vsearch.sh) to run the job. 

Later on, quantify the number of input and output transcripts and record them here. 

### Filter for small transcripts

Use a 300 bp filter to remove small transcripts which are likely incomplete fragments. Use `seqtk` for this with [`filter300bp.sh`](scripts/filter300bp.sh). 

```
module load seqtk

# Filter the clustered centroids for 300 bp sequences (remove them)
seqtk seq -L 300 ../05_Clustering/centroids.fasta > centroids_300bpFilter.fasta

# Pull out the names of the transcripts and send to file
grep ">" centroids_300bpFilter.fasta | sed 's/^>//g' > filtered_centroids.txt

# Subset the protein file that contains all of the sequnces for only the ones that are now centroids and also filtered
seqtk subseq ../04_Coding_Regions/trinity_combine.fasta.transdecoder.pep filtered_centroids.txt > centroids_300bpFilter.pep
```

### Evaluating Assemblies

Now that a final reference transcriptome for each species has been made, they must be evaluated using benchmarking methods. Benchmarking provides an opportunity to evaluate the decisions made to create the reference and an opportunity to learn how well the assembly and reference transcriptome selection process worked. I'll use two common benchmarking tools here: rnaQUAST and BUSCO. I can also use EnTAP's mapping rate information (across both species for a reciprocal approach). 

#### Evaluate with BUSCO

USE BUSCO to evaluate the assembly and annotation quality of both transcriptomes. BUSCO works by comparing your annotation to a conserved set of protein models shared by all members of a group. For me, I'll use the embryophyta as a group. I'll run BUSCO in both transcript and protein mode. The idea for running both is to account for non-synonymous or synonymous variation in the transcripts. The transcript mode uses nucleotide sequences, and the results may be sensitive to synonymous sequence variation. The protein mode, on the other hand, is not sensitive to synonymous changes, but may also miss some of the proteins that were assembled and annotated (I'm not 100% on this statement, but let's do both anyway).

I'll use the scripts [`busco_rna.sh`](scripts/busco_rna.sh) and [`busco_prot.sh`](scripts/busco_prot.sh) to perform the analyses. The core functions for each script are given below:

_`busco_rna.sh`_
```
module load busco/5.0.0

transcriptome=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/centroids.fasta
odb=/isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10
busco -f -i $transcriptome -l $odb -o busco_rna -m tran --offline

```

_`busco_prot.sh`_

```
module load seqtk/1.3 
module load busco/5.0.0

# Set paths of centroids
CENTROIDS=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/centroids.fasta
PEPTIDES=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep

# Use a perl grep to pull out the transcript names without the '>'
grep -oP "(?<=>).*" $CENTROIDS > transcript_ids

# Subset the protein sequences from the transcript id's in the names.txt file 
seqtk subseq $PEPTIDES transcript_ids > centroids.pep

# Perform busco protein run
odb=/isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10
busco -i centroids.pep -l $odb -o busco_protein -m prot --offline
```

The protein run was slightly more complex, requiring the protein sequences to be extracted from using the transcript id's from the reference (i.e. the centroids) file. The subsetting was performed with `seqtk subseq`. 

Results for the BUSCO runs using 1614 BUSCO groups from embryophyta are given below. Overall it seems as if the *chinensis* assembly is slightly better than the *canadensis*. The 300 bp filter appears to have no effect on BUSCO statistics.

_BUSCO Results. Numbers are percents_

| Species | BUSCO Mode | Complete | Complete and single-copy | Complete and duplicated | Fragmented BUSCO | Missing BUSCO | 
| :--- |:--- |:--- |:--- |:--- |:--- |:--- |
| No Transcript Filter | | | | | | |
| canadensis | transcript | 93.7 | 74.1 | 19.6 | 2.5 | 3.8 |
| canadensis | protein | 93.8 |73.9 | 19.9 | 1.7 | 4.5 |
| chinensis | transcript | 94.9 | 74.0 | 20.9 | 1.6 | 3.5 |
| chinensis | protein | 94.9 | 74.3 | 20.6 | 1.3 | 3.8 |
| 300 bp Transcript Filter | | | | | | |
| canadensis | transcript | 93.7 | 74.1 | 19.6 | 2.5 | 3.8 |
| canadensis | protein | 93.8 | 73.9 | 19.9 | 1.7 | 4.5 |
| chinensis | transcript | 94.9 | 74.0 | 20.9 | 1.6 | 3.5 |
| chinensis | protein | 94.9 | 74.3 | 20.6 | 1.3 | 3.8 |

#### Evaluate with rnaQUAST

rnaQUAST is a tool for evaluating RNAseq projects and can be run without a reference genome. The *de novo* mode uses GeneMarkS-T to predict gene models from the transcripts using the `--gene-mark` flag. The gene models are used for QC (_not exactly sure how_). These results were run on the clustered centroids of all of the transcriptomes, and hence, on the reference transcriptome. 

I'll use rnaQUAST to QC the entire pipeline from the first assembly of transcripts to the final reference transcriptome files. The data for these rnaQuast runs is in a [google sheet](https://docs.google.com/spreadsheets/d/1om_nmnPRyAJOtfbrH31O675YoVkO1cPg_fqldHdUEyg/edit#gid=0).

Here are representative bash files for the runs.
- Assemblies: [`rnaQuast_assemblies.sh`](scripts/rnaQuast_assemblies.sh)
- FrameSelection: [`rnaQuast_transDecoder.sh`](scripts/rnaQuast_transDecoder.sh)
- Clustered Centroids: [`rnaQuast_centroids.sh`](scripts/rnaQuast_centroids.sh)

_Basic rnaQUAST transcripts metrics for each clustered centroid ranscriptome (calculated without reference genome and gene database)._

| METRICS/TRANSCRIPTS                     | Can. | Chi. | Can. | Chi. |                
| :--- | :--- | :--- | :--- | :--- |
| Filter | None | None | 300 bp | 300 bp |
| Transcripts                             | 136264     | 321791    | 135894     | 321140    |
| Transcripts > 500 bp                    | 70968      | 153922    | 70968      | 153922    |
| Transcripts > 1000 bp                   | 24893      | 45669     | 24893      | 45669     |
| Average length of assembled transcripts | 725.082    | 659.559   | 726.274    | 660.307   |
| Longest transcript                      | 15570      | 15567     | 15570      | 15567     |
| Total length                            | 98802630   | 212240013 | 98696334   | 212050974 |
| Transcript N50                          | 606        | 486       | 1671       | 807       |

The 300 bp filter removed 370 transcripts from _canadensis_ and 651 from _chinensis_. The N50 of the _canadensis_ reads increased dramatically from 606 to 1671, and the average transcript length increased slightly for both assemblies. 

In addition to the `basic_metrics.txt` output represented in the table, rnaQUAST also generates figures depicting the 'Nx' plot and the transcript length distribution. The 'Nx' plot is defined as "Nx is a maximal number N, such that the total length of all transcripts longer than N bp is at least x% of the total length of all transcripts" (see [here](https://github.com/ablab/rnaquast#sec4.3) for more).

<!-- | Canadensis | Chinensis | -->
<!-- | :---: | :---: | -->
<!-- | ![](figs/Nx_can_centroids.png) | ![](figs/Nx_chi_centroids.png) | -->
<!-- | ![](figs/transcript_length_can_centroids.png) | ![](figs/transcript_length_chi_centroids.png) |  -->

_Nx and transcript length plots for 300 bp filtered assemblies_
| Canadensis | Chinensis |
| :---: | :---: |
| ![](figs/Nx_can_centroids_filter.png) | ![](figs/Nx_chi_centroids_filter.png) |
| ![](figs/transcript_length_can_centroids_filter.png) | ![](figs/transcript_length_chi_centroids_filter.png) |

#### Evaluate with EnTAP

EnTAP is a functional annotation pipeline that also provides benchmarking statistics. The reciprocal blast annotation rate is a usefull tool for evaluating the quality of an annotation. If the predicted protiens are contained within a database of known proteins, EnTAP will return that information as the `Total unique sequences with an alignment`. Take that value and divide it by the total number of sequences. Below is the annotation rate table. The values are fairly low, but most of my experiences are with model species, so this may be fairly normal.

_EnTAP Annotation Rate_

| Dataset | Canadensis | Chinensis |
| :--- | :--- | :--- |
| No Filter | 0.6 | 0.557 |
| 300 bp Filter | 0.6 | 0.562 |


Below are EnTAP statistics for the assemblies using the protein mode (runP) and nucleotide mode (runN).

| | Canadensis   | Canadensis    |  Canadensis     | Canadensis    | Chinensis    | Chinensis      | Chinensis      | Chinensis      |  
| ---: | ---: | ---: | ---:  | ---: |  ---:  | ---:  | ---: | ---: |      
| Filter                                                                    | None         | 300           |  None           | 300           | None         | 300            | None           | 300            |                                                          
| Mode                                                                      | protein      | protein       |  nucleotide     | nucleotide    | protein      | protein        | nucleotide     | nucleotide     |  
| Annotation Rate                                                           | 0.5996888393 | 0.6002472515  |  0.6136176833   | 0.614118357   | 0.56125249   | 0.5616553528   | 0.5715138087   | 0.5718689668   |  
| Total Sequences                                                           | 136264       | 135894        |  136264         | 135894        | 321791       | 321140         | 321791         | 321140         |  
| Total unique sequences with an alignment                                  | 81716        | 81570         |  83614          | 83455         | 180606       | 180370         | 183908         | 183650         |  
| Total unique sequences without an alignment                               | 54548        | 54324         |  52650          | 52439         | 141185       | 140770         | 137883         | 137490         |  
| Total unique sequences with family assignment                             | 106974       | 106787        |  111917         | 111687        | 253059       | 252700         | 263647         | 263220         |  
| Total unique sequences without family assignment                          | 29290        | 29107         |  24347          | 24207         | 68732        | 68440          | 58144          | 57920          |  
| Total unique sequences with at least one GO term                          | 92671        | 92525         |  96900          | 96717         | 223265       | 222960         | 232274         | 231910         |  
| Total unique sequences with at least one pathway (KEGG) assignment        | 36910        | 36828         |  38662          | 38566         | 96870        | 96715          | 100494         | 100321         |  
| Total unique sequences annotated (similarity search alignments only)      | 885          | 879           |  852            | 847           | 1981         | 1974           | 1985           | 1978           |  
| Total unique sequences annotated (gene family assignment only)            | 26143        | 26096         |  29155          | 29079         | 74434        | 74304          | 81724          | 81548          |  
| Total unique sequences annotated (gene family and/or similarity search)   | 107859       | 107666        |  112769         | 112534        | 255040       | 254674         | 265632         | 265198         |  
| Total unique sequences unannotated (gene family and/or similarity search) | 28405        | 28228         |  23495          | 23360         | 66751        | 66466          | 56159          | 55942          |  

The 300 bp filter for the shortest transcripts had a nominal effect on the quality of the transcriptomes. I expected a much larger increase in the annotation rate, but that wasn't observed. The increase of the annotation rate in _canadensis_ is nearly imperceptible, with only 1429 more transcripts mapping in the filtered dataset. The largest difference in QC statistics is the increase of N50 in _canadensis_ from 606 to 1671 after the 300 bp filter. Despite the incremental gains in QC statistics, it still makes sense to use the 300 bp filtered transcriptomes going forward.

### Functional Annotation EnTAP

EnTAP is a functional annotation pipeline that compares your proteins to external databases to find orthologous proteins. EnTAP relies on the annotations in the external databases to functionally annotate your proteins. I will use three databases and the highest ranking annotations among the three are carried forward into the final annotation (`final_results/final_annotations_lvl0.tsv`). I used EnTAP/0.10.8 ([script](scripts/entap-0.10.8.sh)).

I ran EnTAP using these flags and databases, and a protein sequence input file:

```
# Set paths
protein=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/06_Filter/centroids_300bpFilter.pep

# Run EnTAP
EnTAP --runP \
    --ini entap_config.ini \
    -t 32 \
    -i $protein \
    -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd \
    -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
    -d /isg/shared/databases/Diamond/ntnr/nr_protein.202.dmnd
     
```

I will run EnTAP again, but this time with the nucleotides rather than the protein sequences. OK, the `runN` EnTAP runs are going (and finished). The final structural annotation should be `final_annotations_no_contam.tsv` and the contaminant cleaned transcriptome is `final_annotations_no_contam.fnn`. According to the EnTAP manual, those files are, "Nucleotide, protein, and tab-deliminated files containing all annotated sequences that were not flagged as a contaminant." The final sequence file has been filtered of contaminants and contains transcripts that mapped to at least one database. This file leaves out (potentially) real Hemlock transcripts that didn't map to a database. But for now, I don't know if the unannotated transcripts are real, contaiminants, or fragmented genes. If I use the approach of creating a refernce transcript first, then pulling out candidate genes, I'm stuck with this reality. There is another approach to use reads2tree to fish out terpene synthases, and perhaps I'll pursue that path later. 

_Files for downstream analysis:_
```
300M    final_annotations_no_contam.fnn
620M    final_annotations_no_contam_lvl0_enrich_geneid_go.tsv
5.9M    final_annotations_no_contam_lvl0_enrich_geneid_len.tsv
311M    final_annotations_no_contam_lvl0.tsv
599M    final_annotations_no_contam_lvl1_enrich_geneid_go.tsv
5.9M    final_annotations_no_contam_lvl1_enrich_geneid_len.tsv
304M    final_annotations_no_contam_lvl1.tsv
```

### QC

Check the distribution of unmapped genes. They should be very small in distribution in comparison to the mapped genes.

### Final Files for the Transcriptome assemblies

(03/02/23) To obtain the final files for downstream use, I must combine resources from TransDecoder and EnTAP to generate a CDS file. Remember that I ultimately want a CDS file of TPS genes for the selection analysis. The plan Jill and I discussed is to use the original frame selected CDS from TransDecoder and the transcript ids from contaminant and annotated EnTAP analysis. Using a similar approach, I can get other sets of files from the centroid reference transcriptome, TransDecoder gff file, or other files I may want.

__Get the transcript ids to keep__

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/08_EnTAP/filter300bp/entap_outfiles/final_results
grep '>' final_annotations_no_contam.faa | sed 's/^.//' > final_annotations_no_contam.transcript_ids
```
There were 234344 transcript ids in the file, far more than the 135894 EnTAP yielded in the runP log file. There were duplicated transcript ids that were identical. In Vim, remove the duplicates using `:sort u`. Be sure the `>` is removed from the transcript ids.

Take the `final_annotations_no_contam.transcript_ids` and use seqtk's `subseq` to keep the CDS you want from the frame selected output of TransDecoder.  Use the script [cleaned_cds.sh](scripts/cleaned_cds.sh) to do the selection. Here's the command:

```
module load seqtk/1.3

cds=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds
keep_list=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/08_EnTAP/filter300bp/entap_outfiles/final_results/final_annotations_no_contam.transcript_ids

seqtk subseq $keep_list $cds > canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
```

After the `subseq` command, there are 117172 transcripts in canadensis as there should be. In chinensis, there are 231019 transcript ids, which matches the keep file.

#### Final Structural File

To obtain the final structural annotation gff3 file, I must subset the final TransDecoder output file for the transcript IDs I'm keeping. I'll use the AGAT tool [`agat_sp_filter_feature_from_keep_list.pl`](https://agat.readthedocs.io/en/latest/tools/agat_sp_filter_feature_from_keep_list.html). First grab a list of the transcript IDs that are wanted:

```
some unix tool for this > keep.txt
```

The use the perl script to keep them:


```
source activate AGAT

# set up the files
infile=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder_dir/longest_orfs.cds.best_candidates.gff3.revised_starts.gff3
keepers=/path/to/keep_list.txt

agat_sp_filter_feature_from_keep_list.pl --gff $infile --keep_list $keepers -o centroids_filter300bp.gff3

```

Now you can add the gff3 file to the list of final outputs.

Here are the final files for the reference transcriptome, structural annotation gff3, and functional annotation tsv.

**Reference transcriptomes**
```
# Canadensis
/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/08_EnTAP/runN_300bpfilter/entap_outfiles/final_results/final_annotations_no_contam.fnn

# Chinensis
/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/08_EnTAP/runN_300bpfilter/entap_outfiles/final_results/final_annotations_no_contam.fnn

```

**Reference CDS**
```
# Canadensis
/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds

# Chinensis
/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
```

**Structural Annotations**
```
# Canadensis
# Chinensis
```

**Functional Annotation**
```
# Canadensis
/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/08_EnTAP/filter300bp/entap_outfiles/final_result/final_annotations_no_contam_lvl0.tsv

# Chinensis
/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/08_EnTAP/filter300bp/entap_outfiles/final_result/final_annotations_no_contam_lvl0.tsv
```

_I think all the paths above are good. Have to double check at somepoint_

## Protein Searching Methods

Finding TPS enzymes from all of the gene models is not a trivial task. I'll use two complementary approaches to find TPS and p450 enyzmes using 1) sequence similarity, and 2) HMM profile searching.

1. Sequence similarity (EnTAP)
- Compare reference transcriptome protiens (? or nucleotides?) to databses
- Use annotation of sequence in database to assign annotation to unknown reference transcriptomes
- Use EnTAP annotation to find the transcript IDs for the TPS enzymes
  - Search for terms "terpen*" or "Terpen*" to find annotations. 

2. HMM domain profile search
- Rely on the functional annotation to identify TPS genes that contain the core PFAM domains for TPS genes.
- Core PFAM domains are PF01397, PF03936, and PF19086 (according to Li et el. 2018 Prunus persica TPS).
- Subset PFAM domains from the Interproscan results in the Trandsdecoder run.
An example of this is in Li et el. 2018 Prunus persica TPS.
This approach requires some tools to 'fish out' the TPS models based on their PFAM domains.

The transcriptomes were assembled separately for each species and therefore have incompatible naming systems. Use OrthoFinder to create orthogroups between chinensis and canadensis, allowing for homology/orthology between transcripts to be established.
- OrthoFinder analyses between chinensis and canadensis reference transcriptomes to find orthogroups
- Find the orthogroup containing the annotated transcript IDs of interests from both searching methods.
- Pass transcripts on to subsequent analyses. 

### EnTAP

Use the EnTAP annotations to pull transcripts. Search for annotations associated with TPS and p450 enzymes, as well as MEV and MEP pathway. I've got multiple versions of the grep here, and I'm trying to figure out the best way to narrowly select the TPS proteins and not proteins that have 'terpenoid' or 'terpene' in the annotation but are not terpene synthases.

<details>
<summary>The code to grep the functional annotations is here</summary>

```
# Canadensis
cd /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/08_EnTAP/filter300bp/entap_outfiles/final_results
grep -E 'Terpen*|terpen*' final_annotations_no_contam_lvl0.tsv > can_terpene
grep -E 'p450' final_annotations_no_contam_lvl0.tsv > can_p450
grep -E 'mevalonate' final_annotations_no_contam_lvl0.tsv > can_MEV
grep -E 'Terpene_synth*' final_annotations_no_contam_lvl0.tsv > can_Terpene_synth #< This is redundant

# Chinensis
cd /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/08_EnTAP/filter300bp/entap_outfiles/final_results
grep -E 'Terpen*|terpen*' final_annotations_no_contam_lvl0.tsv > chi_terpene
grep -E 'p450' final_annotations_no_contam_lvl0.tsv > chi_p450
grep -E 'mevalonate' final_annotations_no_contam_lvl0.tsv > chi_MEV
grep -E 'Terpene_synth*' final_annotations_no_contam_lvl0.tsv > chi_Terpene_synth

```

</details>

Unfiltered sizes of grep searches. These files need to be reviewed for false positives, expecially in the `_terpene` files. There are words like 'serpentine' or 'perpendicular' which matched the regex `*erpen*`. I decided to use the more limited PFAM term `Terpene_synth*` to get TPS's, and I'll go forward with that file for the next step.

| EnTAP Annotation Set | Transcript count | 
| :--- |---:|
| can_MEV              |   18 | 
| can_p450             |  699 | 
| can_terpene          |  306 | 
| can_Terpene_synth    |  120 | 
| chi_MEV              |   31 | 
| chi_p450             | 1054 | 
| chi_terpene          |  644 | 
| chi_Terpene_synth    |  138 | 

### HMMER

Use protein domains, the input fastam, and HMMER v3 to scan the input for the domains of interest. HMMER takes proteins as input. Convert the `cds` to peptides with seqtk.

```
seqkit translate -F coding.fa > protein.fa
```

1. Obtain a list of PFAM domains you wish to investigate. Use the 3 PFAM domains from Li et al. (2018) plus cytochrome p450.
- Terpene synthase, N-terminal domain, Terpene_synth (PF01397.24)
- Terpene synthase family, metal binding domain, Terpene_synth_C (PF03936.19)
- Terpene synthase family 2, C-terminal metal binding, Terpene_syn_C_2 (PF19086.3)
- Cytochrome p450 (PF00067.25)
- I might not want to include the C-terminal metal binding domain (PF19086.3), as it's not in every TPS gene model and may limit the output. 
- Any other canonical domains? Do I want to include a canonical domain in TPS-d for gymnosperms?

2. Download the HMM's to your home dir: `wget https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz`
- path to hmm database `/home/FCAM/kfetter/pfam/Pfam-A.hmm.gz` 
- No need to uncompress the hmm profile database, hmmr-tools can read zipped files.

3. Create a profile datgabase using `hmmfetch.sh` and the pfam domain ids for the domains of interest.
```
hmm_db=/home/FCAM/kfetter/pfam/Pfam-A.hmm.gz
hmmfetch -f $hmm_db three_tps_domains > tps_3domains.pdb
hmmfetch -f $hmm_db three_tps_domains > p450_3tps_domains.pdb
```

4. Compress and index the profile database
```
hmmpress tps_3domains.pdb
hmmpress p450_3tps_domains.pdb
```

5. Search the target query using the compressed profile database (`hmmscan.sh`)
```
hmmscan --tblout can_tps_3domains.hmmscan.out $profile_db $target_query_can
hmmscan --tblout chi_tps_3domains.hmmscan.out $profile_db $target_query_chi
hmmscan --tblout can_tps_p450_TPS.hmmscan.out $profile_db $target_query_can
hmmscan --tblout chi_tps_p450_TPS.hmmscan.out $profile_db $target_query_chi
```

The output will be a table of hits and their associated p-values and other significance scores. Output of `hmmscan` to use:

```
/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/can_tps_p450_TPS.hmmscan.out
/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/chi_tps_p450_TPS.hmmscan.out
```

#### HMMER Results

The `hmmscan` worked and found 21 TPS transcripts in _canadensis_. Not all of the TPS had all three domains, demonstrating the matches are unions of all three domains. Let's record some data about what I find and then refine the set of domains. The three I chose were from a _Prunus_ paper, and I should consult a gymnosperm manuscript to select additional and/or better domains. After checking a _Pinus_ paper (Diao et al. 2022), the list I have seems sufficient, and I won't add more domains

Table of the 4 domains by species at the transcript level.
| Protein Name | PFAM | Can | Chi |
|:--- |:--- |:--- |:--- |
|Terpene_synth | PF01397.24 | 15 | 32 | 
|Terpene_synth_C | PF03936.19 | 17 | 26 |
|Terpene_syn_C_2 | PF19086.3 | 16 | 22 |
|Cytochrome p450 | PF00067.25 | 153 | 258 |

Table of unique gene-domains by species from HMMER. Each count represents the number of unique domain-gene ids (See code below). This table is more about how many doamins were recovered, not how many genes there are.
|  Domain Name     |    Canadensis  |    Chinensis  |
|:---------- | ---------: | --------: |
|  Terpene_syn_C_2  |           15   |          10   |
|  Terpene_synth    |           13   |          15   |
|  Terpene_synth_C  |           16   |          12   |
|  p450             |           143  |          120  |

Table of unique genes found for either TPS or p450. Recall that some TPS have multiple domains. This is a count of just the unique list of genes for each species. Presumably there are orthologs here, so the number of genes is less than the sum of the two speices

| Protein Type |    Canadensis  |    Chinensis  |
|:---------- | ---------: | --------: |
|  Terpene     |           19   |          20   |
|  p450        |           143  |          120  |

<details>
<summary>Scratch for gene count table:</summary>

```R
library(tidyverse)
can = read.table("can_4",T,'\t')
chi = read.table("chi_4",T,'\t')

# Count distinct entries
can %>% distinct(name, gene_id, .keep_all=TRUE) %>% select(name) %>% table()
chi %>% distinct(name, gene_id, .keep_all=TRUE) %>% select(name) %>% table()

# This method of counting combines the gene id with the domain. 
# Otherwise, the output reports the first domain in alphabetical order.

# How many unique genes were identified for TPS, for p450? By species.
can %>% filter(name != 'p450') %>% distinct(gene_id) %>% dim() # 19
chi %>% filter(name != 'p450') %>% distinct(gene_id) %>% dim() # 20
can %>% filter(name == 'p450') %>% distinct(gene_id) %>% dim() # 143 
chi %>% filter(name == 'p450') %>% distinct(gene_id) %>% dim() # 120

```

</details>

<details>
<summary>Which genes have which domains?</summary>

```R
can_tps <- can %>% filter(name != 'p450')
table(can_tps$gene_id, can_tps$domain)
#                               PF01397.24 PF03936.19 PF19086.3
#  ACN_01_TRINITY_DN187_c0_g1            1          1         1
#  ACN_01_TRINITY_DN36345_c0_g1          1          0         0
#  ACN_01_TRINITY_DN38968_c0_g1          0          0         1
#  ACN_01_TRINITY_DN5904_c0_g1           0          1         0
#  ACN_01_TRINITY_DN8033_c0_g1           0          0         1
#  ACN_02_TRINITY_DN10530_c0_g1          1          1         1
#  ACN_02_TRINITY_DN176_c0_g1            2          1         1
#  ACN_02_TRINITY_DN1779_c0_g1           1          1         0
#  ACN_02_TRINITY_DN22506_c0_g2          0          1         1
#  ACN_02_TRINITY_DN7895_c0_g1           2          2         2
#  ACN_02_TRINITY_DN8582_c0_g1           0          1         1
#  ACN_02_TRINITY_DN8790_c0_g1           1          1         1
#  ACN_02_TRINITY_DN9390_c0_g1           1          1         1
#  JSP_01_TRINITY_DN4682_c0_g1           1          1         0
#  JSP_01_TRINITY_DN4682_c0_g4           1          1         1
#  JSP_01_TRINITY_DN4682_c0_g5           1          1         1
#  JSP_01_TRINITY_DN5141_c0_g1           0          1         1
#  JSP_02_TRINITY_DN2883_c0_g1           1          1         1
#  JSP_02_TRINITY_DN3218_c0_g2           1          1         1
chi_tps <- chi %>% filter(name != 'p450')
table(chi_tps$gene_id, chi_tps$domain)
#                              PF01397.24 PF03936.19 PF19086.3
# ACH_01_TRINITY_DN11517_c0_g1          2          2         0
# ACH_01_TRINITY_DN41030_c0_g1          0          2         0
# ACH_01_TRINITY_DN5168_c0_g1           2          2         2
# ACH_01_TRINITY_DN6523_c0_g1           0          0         2
# ACH_02_TRINITY_DN16265_c0_g1          2          0         0
# ACH_02_TRINITY_DN3307_c0_g1           4          4         4
# ACH_02_TRINITY_DN3307_c0_g2           2          0         0
# ACH_02_TRINITY_DN3307_c1_g1           2          2         2
# ACH_02_TRINITY_DN5461_c0_g2           2          0         0
# ACH_02_TRINITY_DN5770_c0_g1           2          2         2
# ACH_03_TRINITY_DN33076_c0_g1          2          2         2
# ACH_03_TRINITY_DN5770_c1_g1           0          2         2
# ACH_03_TRINITY_DN8327_c0_g1           0          2         2
# ACH_03_TRINITY_DN8327_c0_g2           2          0         0
# ACH_03_TRINITY_DN8469_c0_g1           2          2         2
# ACH_04_TRINITY_DN22290_c0_g1          2          0         0
# ACH_04_TRINITY_DN7114_c0_g1           0          2         0
# ACH_04_TRINITY_DN9055_c0_g1           2          0         0
# ACH_05_TRINITY_DN15765_c0_g1          2          0         0
# ACH_05_TRINITY_DN20794_c0_g1          2          2         2
# somewhat odd they all have 2 domains.
# I probably doubled the file at some point, hunt this down.


```

</details>

### Comparing gene lists

Now that I have a list of hmmer and EnTAP transcripts, I want to know how similar the lists are. Using tools in R, compare the lists of genes and find the overlap between sets. Get an understanding for the overall overlap between methods at the gene level, then break it down into the TPS/p450, and finally the domain level. It's likely that EnTAP and HMMER recovered different domains entirely. I can make a single UpSetR plot that will display different levels of overlap (if I want).

<details>
<summary>From the hmmer output, format the file into a tsv. Create a column of query names that remove the elements not needed and create an isoform, transcript, and gene column. Paste the columns in to keep the other statistics. Also prepare the EnTAP files.</summary>

```
# Make the dir and copy the files
cd /core/labs/Wegrzyn/karl/tsuga_sel/protein_search/
mkdir compare
cd compare

# Using vim tools, create a text file of the hmmscan.out that includes the genes, isoforms, and other data as a tsv
# Steps not recorded.
# Copy them here
cp /core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/compare/can_tps_p450_TPS.hmmscan.out.txt .
cp /core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/compare/chi_tps_p450_TPS.hmmscan.out.txt .

# Prepare the EnTAP files
cd /core/labs/Wegrzyn/karl/tsuga_sel/protein_search/entap
cut -f1 can_terpene > can_terpene.full-name
cut -f1 chi_terpene > chi_terpene.full-name
cut -f1 can_p450 > can_p450.full-name
cut -f1 chi_p450 > chi_p450.full-name

# Make isoform/gene ids out of the full_names in the transcript file
cp can_terpene.full-name can_terpene.isoform
cp can_terpene.full-name can_terpene.gene
sed -i 's/\.p[0-9].*$//g' can_terpene.isoform
sed -i 's/\_i[0-9].*$//g' can_terpene.gene
paste can_terpene.gene can_terpene.isoform can_terpene.full-name > can_terpene.out.txt
awk 'BEGIN {print "gene\tisoform\tfull-name"} 1' can_terpene.out.txt | cat - can_terpene.out.txt  > temp && mv temp can_terpene.out.txt 

cp chi_terpene.full-name chi_terpene.isoform
cp chi_terpene.full-name chi_terpene.gene
sed -i 's/\.p[0-9].*$//g' chi_terpene.isoform
sed -i 's/\_i[0-9].*$//g' chi_terpene.gene
paste chi_terpene.gene chi_terpene.isoform chi_terpene.full-name > chi_terpene.out.txt
awk 'BEGIN {print "gene\tisoform\tfull-name"} 1' chi_terpene.out.txt | cat - chi_terpene.out.txt  > temp && mv temp chi_terpene.out.txt 

cp can_p450.full-name can_p450.isoform
cp can_p450.full-name can_p450.gene
sed -i 's/\.p[0-9].*$//g' can_p450.isoform
sed -i 's/\_i[0-9].*$//g' can_p450.gene
paste can_p450.gene can_p450.isoform can_p450.full-name > can_p450.out.txt
awk 'BEGIN {print "gene\tisoform\tfull-name"} 1' can_p450.out.txt | cat - can_p450.out.txt  > temp && mv temp can_p450.out.txt 

cp chi_p450.full-name chi_p450.isoform
cp chi_p450.full-name chi_p450.gene
sed -i 's/\.p[0-9].*$//g' chi_p450.isoform
sed -i 's/\_i[0-9].*$//g' chi_p450.gene
paste chi_p450.gene chi_p450.isoform chi_p450.full-name > chi_p450.out.txt
awk 'BEGIN {print "gene\tisoform\tfull-name"} 1' chi_p450.out.txt | cat - chi_p450.out.txt  > temp && mv temp chi_p450.out.txt 

p *.out.txt ../compare/
cd ../compare
R
```

</details>

<details>
<summary>Now that the files are ready, read them into R and compare the genes, and protein domains found by each method.</summary>

```R
# Read in the TPS/p450 sets
setwd("/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/compare")
can_entap_tps  <- read.table("can_terpene.out.txt",T,'\t')
chi_entap_tps  <- read.table("chi_terpene.out.txt",T,'\t')
can_entap_p450 <- read.table("can_p450.out.txt",T,'\t')
chi_entap_p450 <- read.table("chi_p450.out.txt",T,'\t')
can_hmmr       <- read.table("can_tps_p450_TPS.hmmscan.out.txt",T,'\t')
chi_hmmr       <- read.table("chi_tps_p450_TPS.hmmscan.out.txt",T,'\t')

# Add some metadata to the entap files
 can_entap_tps$method = "entap"
 chi_entap_tps$method = "entap"
can_entap_p450$method = "entap"
chi_entap_p450$method = "entap"

 can_entap_tps$pseudo_target_name = "terpenoid"
 chi_entap_tps$pseudo_target_name = "terpenoid"
can_entap_p450$pseudo_target_name = "p450"
chi_entap_p450$pseudo_target_name = "p450"

# Create a list of the sets
gene <- list(
  can_entap_tps = can_entap_tps$gene,
  chi_entap_tps = chi_entap_tps$gene,
  can_hmmr_tps = subset(can_hmmr, target_name %in% c("Terpene_syn_C_2", "Terpene_synth", "Terpene_synth_C"))$gene,
  chi_hmmr_tps = subset(chi_hmmr, target_name %in% c("Terpene_syn_C_2", "Terpene_synth", "Terpene_synth_C"))$gene,
  can_entap_p450 = can_entap_p450$gene,
  chi_entap_p450 = chi_entap_p450$gene,
  can_hmmr_p450 = subset(can_hmmr, target_name %in% c("p450"))$gene,
  chi_hmmr_p450 = subset(chi_hmmr, target_name %in% c("p450"))$gene)


# Set the labels for the sets
labels = c("can_entap_tps", "chi_entap_tps", "can_hmmr_tps", "chi_hmmr_tps", "can_entap_p450", "chi_entap_p450", "can_hmmr_p450", "chi_hmmr_p450")

# Create the UpSetR plot
upset_obj <- upset(fromList(gene), 
                   sets = labels,
                   order.by = "freq",
                   point.size = 3,
                   mainbar.y.label = "Number of Genes",
                   sets.bar.color = "#56B4E9",
                   text.scale=1.25)

# Save the plot as a PNG file
png("upset_plot_gene_all_v1.png", width = 2000, height = 1400, units = "px", res = 250)
print(upset_obj)
dev.off()

```

</details>

<img src="figs/upset_plot_gene_all.png" alt="Figure" width="60%">

__Upset plot comparing TPS and p450 genes found in an HMMER scan and EnTAP functional annotation. Some EnTAP results are likely to contain false positives.__

Some of the EnTAP results are likely false positives as a result of how they were obtained. I grepped 'Terpen*' or 'terpen*' which included any functional annotation with those phrases. After looking at some of the resulsts, there are false positives there. To remove them, I'll make a genetic PCA from the sequences. It should be obvious which genes are false positives. After the PCA plot is made, remake the UpSet plot and include it in the manuscript/supplemental.

#### Candidate transcript sizes

How do the sizes of transcripts differ between EnTAP and HMMER results?


### Filtering False Positives

I know there are false positives in the EnTAP lists. These are transcripts that are fragments, and off-target grep hits. Identifying the genes to keep may be challenging. My strategy will be to cluster the candidate genes with themselves and with known TPS/p450 genes. Clusters with singletons will be investigated and dropped. What criteria will I use for dropping: singletons without domain annotation (PFAM/InterPro/etc). 

Bring in TPS/p450 from known _Pinus_, _Populus_, and possibly _Arabidopsis_. Create a set of sequences that are representative of TPS and p450 genes. This is not an exhaustive list. Obtain CDS for each gene.

Table of manuscripts consulted
| Species | Citation | Link |
| :--- | :--- | :---|
| Pinus taeda | | |

<details>
<summary>The canonical gene lists are here:</summary>

```
 

```

</details>




To identify them, I'll make a PCA of the candidate TPS and p450 genes. 

#### Prepare the data for alignment

<details>
<summary>Create meta-data for a list of seqs for input selection. (create_input_list.sh)</summary>

```
# Array of file names
files=("can_p450.out.txt" "chi_terpene.out.txt" "can_terpene.out.txt" "chi_p450.out.txt")

# Print the header names to the output file
echo -e "species\tprotein_name\ttranscript" > output.txt

# Iterate over each file
for file in "${files[@]}"; do
    # Extract the base name without the extension
    base_name=$(basename "$file" .out.txt)

    # Determine the species name and protein name based on the file name
    if [[ $base_name == can_p450* ]]; then
        species_name="canadensis"
        protein_name="p450"
    elif [[ $base_name == chi_terpene* ]]; then
        species_name="chinensis"
        protein_name="terpene"
    elif [[ $base_name == can_terpene* ]]; then
        species_name="canadensis"
        protein_name="terpene"
    elif [[ $base_name == chi_p450* ]]; then
        species_name="chinensis"
        protein_name="p450"
    fi

    # Get the third column of the file (excluding the header)
    third_column=$(tail -n +2 "$file" | cut -f3)

    # Print the columns as separate lines with species name, protein name, and third column
    paste <(yes "$species_name" | head -n $(wc -l < "$file")) <(yes "$protein_name" | head -n $(wc -l < "$file")) <(echo "$third_column")

done >> output.txt

mv output.txt input_list.metadata

```

</details>


<details>
<summary>Separate the metadata into p450 and terpene, then take the first column as input for seqtk (make_seqtk_input.sh). Remove duplicates</summary>

```
# Separate metadata into p450 and terpene files
grep "p450" input_list.metadata | cut -f3 | awk '!seen[$0]++' > p450_list.txt
grep "terpene" input_list.metadata | cut -f3 | awk '!seen[$0]++' > terpene_list.txt

```

</details>

I'm not sure if the alignment should be separate for the p450/TPS or if one alignment is best, so I'll do both.

First Make separate fasta files for the TPS and p450 transcripts. Edit the headers to include some basic annotations for use later in plotting. Script for pulling seqs ([pull_seqs.sh](scripts/pull_seqs.sh))


<details>
<summary>Using seqtk's grep, pull out the sequences you want.</summary>

```
# Make the input names
can_input=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
chi_input=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
terp_list=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/compare/terpene_list.txt
p450_list=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/compare/p450_list.txt

# Create fasta for each species x protein
seqkit grep -f $terp_list $can_input > terpenoid_can.fa
seqkit grep -f $p450_list $can_input > p450_can.fa
seqkit grep -f $terp_list $chi_input > terpenoid_chi.fa
seqkit grep -f $p450_list $chi_input > p450_chi.fa


```

</details>

<details>
<summary>Edit the headers to add the species and protein name</summary> 

```
sed -i -E 's/^>(.*)$/>\1 canadensis p450/' p450_can.fa
sed -i -E 's/^>(.*)$/>\1 chinensis p450/' p450_chi.fa
sed -i -E 's/^>(.*)$/>\1 canadensis terpene/' terpenoid_can.fa
sed -i -E 's/^>(.*)$/>\1 chinensis terpene/' terpenoid_chi.fa

```

</details>

<details>
<summary>Now, merge the individual files together that have the annotation (merge.sh)</summary>

```

# Merge the files by protein
seqkit concat -f terpenoid_can.fa terpenoid_chi.fa > merged_terpene_can_chi.fa
seqkit concat -f p450_can.fa p450_chi.fa > merged_p450_can_chi.fa
seqkit concat -f merged_terpene_can_chi.fa merged_p450_can_chi.fa > merged_all.fa

```

</details>

#### VSEARCH Clustering

To identify false positives, use make clusters of the genes with VSEARCH. Then see if you can rationally remove some of them.

<details>
<summary>Simplify the header names for easier work</summary>

```
sed -i -E '/^>/ s/([^ ]+).* (canadensis|chinensis) (.*$)/\1_\2_\3/' merged_p450_can_chi.fa  
sed -i -E '/^>/ s/([^ ]+).* (canadensis|chinensis) (.*$)/\1_\2_\3/' merged_terpene_can_chi.fa  

# Commands not executed:
sed -i -E '/^>/ s/([^ ]+).* (canadensis|chinensis) (.*$)/\1_\2_\3/' terpenoid_chi.fa
sed -i -E '/^>/ s/([^ ]+).* (canadensis|chinensis) (.*$)/\1_\2_\3/' terpenoid_can.fa
sed -i -E '/^>/ s/([^ ]+).* (canadensis|chinensis) (.*$)/\1_\2_\3/' p450_chi.fa
sed -i -E '/^>/ s/([^ ]+).* (canadensis|chinensis) (.*$)/\1_\2_\3/' p450_can.fa

```

</details>

<details>
<summary>Use VSEARCH cluster to find clusters within each protein type</summary>


```
module load vsearch/2.22.1

vsearch --cluster_fast ../merged_terpene_can_chi.fa --id 0.8 \ 
        --centroids terpene_centroids.fa --clusterout_id \
        --clusterout_sort --threads 8 \
        --biomout terpene.biomout --uc terpene_uc --otutabout terpene_otutable

```

</details>

I might need to relax the matching percent (--id). My clusters don't mix across species. By redcing the matching rate, I may get clusters that represent orthologs. Another way to do this is to use OrthoFinder.


#### Multiple sequence alignment (archive)

I would prefer to use COATi to make the alignments, but it's a bit more complicated than I care to do now. Use CLUSTALW and COBALT to make the alignments. These are online tools, so I won't record the steps here.

Merged All clustalW: https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20230709-215955-0640-54764281-p1m&analysis=text-summary
Terpenoid clustalW: https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20230710-002054-0830-95276084-p1m&analysis=text-summary
p450 clustalW: 


#### PCA/Tree

<details>
<summary>Scratch</summary>

1. Make a metadata file for the genetic PCA
    ```
    cut -d ' ' -f 1,9,10 merged_all_msa.clustalw.fa | sed -r 's/^>//' > metadata_merged_all.fa.txt
    ````

2. Simplify the headers
    ```
    sed -i -E 's/([^ ]+).*/\1/' merged_all_msa.clustalw.fa 
    ````

3. Using R, do this:
    ```R




    ```

</details>


### Final TPS/p450 gene lists

The final set of candidate transcripts for tps and p450` are here:
```

```

TODO: __In the supplement, send the hmmr results there as a seperate file. They are useful as they have the e-values of the identifications and other useful information.__

## Identify/Annotate candidate TPS genes in Hemlocks. 

Develop a diversity panel of TPS sequences from the literature. Use the set of sequences to built a phylogenetic tree and include the Tsuga sequences. Use the result to annotate my sequences. Download TPS enzymes from several model plant genomes. Make records of what you download and from where. 

To annotate the new hemlock TPS genes, I'll identify orthologs to well-known canonical TPS gene models from several model systems. Orthologs will be identified with 1) phylogenetic comparaison, 2) sequence clustering with VSEARCH, and if neccessary, 3) OrthoFinder. A total evidence approach will be used to annotate the hemlock TPS for canonical TPS classification. 

### Obtain canonical TPS genes

Arabidopsis TPS were used to seed a search. Using Table S7 from [Zhou and Pichersky](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7422722/), full length, functionally characterized TPS proteins were downloaded from (TAIR/PHYTOZOME/ETC.). TPS orthologs in five species were identified and their CDS (or protein) sequences were downloaded from InterPro. Confier TPS were identified from [Keeling et al. (2011)](https://www.frontiersin.org/articles/10.3389/fpls.2019.01166/full), and sequences downloaded from Phytozome. Full length, funtionally characterized TPS proteins of Tomato were identified in Table S7 of Zhou and Pickerstky (2020).

Build a phylogeny from the diversity panel and include my hemlock sequences.

#### Scratch for Obtaining TPS for phylogeny

#### Arabidopsis

Download the list of genes from TAIR using their search tool. Change the headers for the files to include the short name after the locus id.

#### Tomato

<details>
<summary>Download the tomato genome and then use seqkit or another method to pull out the desired genes by their name</summary>

```
# Download from Phytozome the SL4.0 tomato genome
curl --cookie jgi_session=/api/sessions/36077d8b1f695a48769841ee6e5b758e --output download.20230717.233538.zip -d "{\"ids\":{\"Phytozome-390\":[\"585474607ded5e78cff8c488\",\"585474637ded5e78cff8c48c\"]}}" -H "Content-Type: application/json" https://files.jgi.doe.gov/filedownload/

# Using the list of TPS genes from Zhou 2020, subset the cds/protein fasta for them
seqkit grep --by-name -r -f input Slycopersicum_390_ITAG2.4.cds_primaryTranscriptOnly.fa > Slycopersicum_tps.cds.fa
seqkit grep --by-name -r -f input Slycopersicum_390_ITAG2.4.protein_primaryTranscriptOnly.fa > Slycopersicum_tps.pep.fa

```

</details>

<details>
<summary>Use R to merge the short annotation names to Zhou's table S7 to get more complete headers</summary>

```R
setwd("/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tomato")
file1 = read.table("file1",T,'\t')
file4 = read.table("file4",T,'\t')
out = merge(file1, file4, by.x="gene_id", by.y="Locus", all.x=T)

```

</details>

<details>
<summary>Replace the header of the tomota tps fasta files with the full header that includes TPS and TPS-clade</summary>

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tomato
# When you return figure out how to replace the headers of the tomatao tps fasta with the headers in new header
# be sure to match for headers, the arean't in the right order in new header.

# Get the old headers into a file to use for sorting
grep '>' Slycopersicum_tps.cds.fa > old_header

# Use vim to get a file with 2 columns, one with the full header and another with the reduced header

# Sort the new headers by the order in the old header
old = read.table("old_header.txt",F,'\t')
new = read.table("new_header.txt",F,'\t')
out = new[match(old$V1, new$V1),]

# Replace the headers
awk '/^>/ { getline new < "sorted_headers"; print new; next } { print }' Slycopersicum_tps.cds.fa > updated.fasta
awk '/^>/ { getline new < "sorted_headers"; print new; next } { print }' Slycopersicum_tps.pep.fa > updated.pep.fasta

# Fix the heaers by adding the '>' back to the beginning (oops)
sed -i 's/^S/>S/' Slycopersicum_tps.cds.fa 
sed -i 's/^S/>S/' Slycopersicum_tps.pep.fa 

```

</details>

#### Populus trichocarpa

Using the list of genes that were amplified in Irmish et al. 2014, download their cds/protein from Genbank. Record accession numbers.

#### Physcomitrium patens

The single complete terpene synthas (CPS/KS) was downloaded from [Keeling et al. 2010](https://doi.org/10.1104/pp.109.151456).

#### Picea sp.

TPS from Picea were downloaded from NCBI using the list provided by [Keeling et al. 2011](https://doi.org/10.1186%2F1471-2229-11-43).

#### Tusga sp.

<details>
<summary>Using the results of the HMMER scans (*out) files, take the column with the transcript ids, remove the text after .p1, then use seqkit to grep the TPS sequences.</summary>

```
# grab_tps.sh
canadensis_pep=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.pep.fa
canadensis_cds=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
chinensis_pep=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.pep.fa
chinensis_cds=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds

seqkit grep --by-name -r -f list_can_tps $canadensis_pep > can_tps.pep.fa
seqkit grep --by-name -r -f list_can_tps $canadensis_cds > can_tps.cds.fa
seqkit grep --by-name -r -f list_chi_tps $chinensis_pep > chi_tps.pep.fa
seqkit grep --by-name -r -f list_chi_tps $chinensis_cds > chi_tps.cds.fa

sed -i 's/_frame=1//' can_tps.pep.fa
sed -i 's/_frame=1//' can_tps.cds.fa
sed -i 's/_frame=1//' chi_tps.pep.fa
sed -i 's/_frame=1//' chi_tps.cds.fa

```

</details>

#### Concatenate TPS fastas

<details>
<summary>Concatenate the TPS fasta files from the various species into a single file.</summary>

```
#cat_cds.sh
arabi=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/arabidopsis/arabidopsis_thaliana_tps.cds.fa
conifer=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/conifer/picea_sp_tps_newHeader.cds.fa
physco=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/physcomitrium/physcomitrium_tps.cds.fa
populus=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/populus/populus_tps_ncbi.cds.fa
tomato=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tomato/Slycopersicum_tps.cds.fa
tchi=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tsuga/chi_tps.cds.fa
tcan=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tsuga/can_tps.cds.fa 

cat $arabi $conifer $physco $populus $tomato $tchi $tcan > tps_7species.cds.fa 

# Remove the '*' that are there for some reason
sed -i 's/\*//' tps_7species.cds.fa

# Simplify the headers
sed -i 's/\s.*$//' tps_7species.cds.fa

# Set wrap to 80 characters
seqkit seq -w 80 <(cat tps_7species.cds.fa) > tps_7species.cds.fa

```

</details>

<details>
<summary>Repeat the concat for the protein files.</summary>

```

arabi=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/arabidopsis/arabidopsis_thaliana_tps.pep.fa
conifer=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/conifer/picea_sp_tps_newHeaders.pep.fa
physco=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/physcomitrium/physcomitrium_tps.pep.fa
populus=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/populus/populus_tps_ncbi.pep.fa
tomato=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tomato/Slycopersicum_tps.pep.fa
tchi=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tsuga/chi_tps.pep.fa
tcan=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tsuga/can_tps.pep.fa 

cat $arabi $conifer $physco $populus $tomato $tchi $tcan > tps_7species.pep.fa 

# Remove the '*' that are there for some reason
sed -i 's/\*//' tps_7species.pep.fa

# Simplify the headers
sed -i 's/\s.*$//' tps_7species.pep.fa

# Set wrap to 80 characters
seqkit seq -w 80 <(cat tps_7species.pep.fa) > tps_7species.pep.fa

```

</details>

### VSEARCH Sequence Clustering

Using [VSEARCH](https://github.com/torognes/vsearch) I will attempt to refine my understanding of the genes I have. Ideally, I will be able to name the _Tsuga_ orthologs with the TPS numbering system. In the TPS-d family, genes are named according to the molecule they create. My Spruce TPS enyzmes have been annotated for their canonical TPS-d name, and hopefully I can get my TPS to cluster with them. I'll try a vareity of clustering sequence similarity values to observe how many singlestons and sequences per cluster I obtain. I'll look at the distribution and try to rationally pick a best supported sequence similarity value.  

First a header issue:

<details>
<summary>Remove the period in the headers so vsearch includes the whole header is included in the vsearch output</summary>

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps
sed -i 's/\./_/' tps_7species.cds.fa 
sed -i 's/\./_/' tps_7species.pep.fa 

```

</details>

<details>
<summary>The column names may be too long for the Spruce samples, make them shorter.</summary>

```

```

</details>




<details>
<summary>Use VSEARCH to cluster sequences. Include the canonical sequences from the phylogeny. Include sequence similarities from 0.6 to 0.9</summary>

```
module load vsearch/2.22.1

sequences=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tps_7species.cds.fa

vsearch --cluster_fast $sequences --id 0.45 --centroids terpene_centroids_id45.fa --clusterout_sort --clusterout_id --log LOGFile45 --threads 16 --biomout terpene_7sp_id45.biomout --uc terpene_uc_7sp_id45 --otutabout terpene_7sp_id45_otutable

vsearch --cluster_fast $sequences --id 0.5 --centroids terpene_centroids_id50.fa --clusterout_sort --clusterout_id --log LOGFile50 --threads 16 --biomout terpene_7sp_id50.biomout --uc terpene_uc_7sp_id50 --otutabout terpene_7sp_id50_otutable

vsearch --cluster_fast $sequences --id 0.55 --centroids terpene_centroids_id55.fa --clusterout_sort --clusterout_id --log LOGFile55 --threads 16 --biomout terpene_7sp_id55.biomout --uc terpene_uc_7sp_id55 --otutabout terpene_7sp_id55_otutable

vsearch --cluster_fast $sequences --id 0.6 --centroids terpene_centroids_id60.fa --clusterout_sort --clusterout_id --log LOGFile60 --threads 16 --biomout terpene_7sp_id60.biomout --uc terpene_uc_7sp_id60 --otutabout terpene_7sp_id60_otutable

vsearch --cluster_fast $sequences --id 0.7 --centroids terpene_centroids_id70.fa --clusterout_sort --clusterout_id --log LOGFile70 --threads 16 --biomout terpene_7sp_id70.biomout --uc terpene_uc_7sp_id70 --otutabout terpene_7sp_id70_otutable

vsearch --cluster_fast $sequences --id 0.75 --centroids terpene_centroids_id75.fa --clusterout_sort --clusterout_id --log LOGFile75 --threads 16 --biomout terpene_7sp_id75.biomout --uc terpene_uc_75sp_id70 --otutabout terpene_7sp_id75_otutable

vsearch --cluster_fast $sequences --id 0.8 --centroids terpene_centroids_id80.fa --clusterout_sort --clusterout_id --log LOGFile80 --threads 16 --biomout terpene_7sp_id80.biomout --uc terpene_uc_7sp_id80 --otutabout terpene_7sp_id80_otutable

vsearch --cluster_fast $sequences --id 0.85 --centroids terpene_centroids_id85.fa --clusterout_sort --clusterout_id --log LOGFile85 --threads 16 --biomout terpene_7sp_id85.biomout --uc terpene_uc_7sp_id85 --otutabout terpene_7sp_id85_otutable

vsearch --cluster_fast $sequences --id 0.9 --centroids terpene_centroids_id90.fa --clusterout_sort --clusterout_id --log LOGFile90 --threads 16 --biomout terpene_7sp_id90.biomout --uc terpene_uc_7sp_id90 --otutabout terpene_7sp_id90_otutable

```

</details>

VSEARCH found multiple clusters of various sizes given below:

<img src="figs/clusters_vs_sequence_similarity_v0.png" alt="Figure" width="50%">

And here with tomoato removed

<img src="figs/clusters_vs_sequence_similarity_v1.png" alt="Figure" width="50%">

Take a closer look at the results, specifically the result with six clusters and one singleton cluster.

## OrthoFinder

This step is to assign orthology between transcripts so I can assign a common set of names. Since the naming convention of Trinity is random, the orthogroups will be used to assign homologus genes based on their protein seuqence. I should be able to use OrthoFinder to get one-to-one orthology for a transcript between species. After this step, I can make a codon-aware alignment for each orthologous pair of transcripts for all 41 individuals.

### OrthoFinder: Complete CDS

Perform an OrthoFinder analysis on the complete CDS for both hemlock species. The goal here is to identify orthologs of the complete CDS in order to remove spurious results from partially assembled, or incorrectly assembled transcripts.

<details>
<summary>Filter for complete cds. Use the python script and bash script to filter for complete cds.</summary>

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/orthofinder/complete_cds 

# filter_complete.py
import sys
from Bio import SeqIO
import os

def filter_complete_sequences(input_file, output_file):
    with open(output_file, 'w') as out_handle:
        for record in SeqIO.parse(input_file, 'fasta'):
            if 'complete' in record.description:
                SeqIO.write(record, out_handle, 'fasta')

def main():
    if len(sys.argv) != 2:
        print("Usage: python filter_complete.py /path/to/fasta.pep")
        sys.exit(1)

    input_file = sys.argv[1]
    base_name, extension = os.path.splitext(os.path.basename(input_file))
    output_file = os.path.join(os.getcwd(), base_name + '_complete' + extension)

    filter_complete_sequences(input_file, output_file)
    print(f"Complete sequences have been saved to {output_file}")

if __name__ == "__main__":
    main()

#filter.sh
ref_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.pep.fa
ref_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.pep.fa

python filter_comlete.py $ref_can
python filter_comlete.py $ref_chi

# Fix the names (outside of filter.sh)
mv canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.pep_complete.fa canadensis_ref_complete.pep.fa
mv chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.pep_complete.fa chinensis_ref_complete.pep.fa

```

</details>

<summary>Run OrthoFinder on the complete cds</summary>

```
module load OrthoFinder/2.5.4

python $ORTHOFINDER -t 16 -f /core/labs/Wegrzyn/karl/tsuga_sel/orthofinder/complete_cds
```

<details>
<summary>Here is a list of the orthologs of TPS genes from the complete cds OrthoFinder run.</summary>

```
# tps_in_OGs.sh
can_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_can_tps
chi_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_chi_tps

phylo_orthogroups=/core/labs/Wegrzyn/karl/tsuga_sel/orthofinder/complete_cds/OrthoFinder/Results_Aug19/Phylogenetic_Hierarchical_Orthogroups/N0.tsv

# Find the phylogenetic hierarchichal ortogroups
grep -f $can_tps $phylo_orthogroups > can_phylo_OGs
grep -f $chi_tps $phylo_orthogroups > chi_phylo_OGs

# location: /core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/OG0001134/can_phylo_OGs
# location: /core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/OrthoFinder_result/

```

_Table of orthologs containing full length TPS cds from the eastern and chinese hemlocks._

| OrthoGroup | OG Type Can-to-Chi |  Canadensis Orthogroup Members | Chinensis Orthogroup Members |   
| :--- | :--- | :--- | 
| OG0000090 | Many:0 | JSP_01_TRINITY_DN4682_c0_g1_i6.p1, JSP_01_TRINITY_DN4682_c0_g4_i1.p1, ACN_01_TRINITY_DN187_c0_g1_i28.p1, JSP_02_TRINITY_DN3218_c0_g2_i2
| OG0001134 | 1:Many | ACN_02_TRINITY_DN1779_c0_g1_i4.p1 | ACH_04_TRINITY_DN7114_c0_g1_i5.p1, ACH_01_TRINITY_DN11517_c0_g1_i3.p1 | 
| OG0001134 | 1:1    | ACN_02_TRINITY_DN9390_c0_g1_i1.p1 | ACH_02_TRINITY_DN5770_c0_g1_i25.p1 | 
| OG0003317 | Many:1 | ACN_01_TRINITY_DN38968_c0_g1_i1.p1, ACN_01_TRINITY_DN41468_c0_g1_i1.p1 | ACH_01_TRINITY_DN25455_c0_g1_i1.p1 | 
| OG0003764 | 1:Many | ACN_01_TRINITY_DN8033_c0_g1_i2.p1 | ACH_01_TRINITY_DN12605_c0_g1_i2.p1, ACH_01_TRINITY_DN6523_c0_g1_i8.p1 | 
| OG0003909 | Many:1 | ACN_02_TRINITY_DN176_c0_g1_i4.p1, ACN_02_TRINITY_DN176_c0_g1_i8.p1 | ACH_02_TRINITY_DN5461_c0_g2_i1.p1 | 
| OG0004264 | 0:Many | ACH_03_TRINITY_DN8327_c0_g1_i2.p1, ACH_01_TRINITY_DN5168_c0_g1_i7.p1, ACH_02_TRINITY_DN3307_c1_g1_i5.p1 |



</details>

</details>

## Multiple sequence alignment

Prepare files for multiple sequence alignment (MSA). Create a fasta for each TPS gene and 50 random genes by species. Then use the OrthoFinder results to establish homology and concatenate the fasta files so that one file per gene contains both species. Use [COATi](https://github.com/CartwrightLab/coati#installation) to make the MSA. COATi is installed on my Linux and Mac machines, but not xanadu. Also try MAFFT using xanadu and the online version of it. 

### Complete CDS FASTA files

Using the `orthofinder/complete_cds` run, get some information on the number of TPS genes in orthogroups.

<details>
<summary>How many TPS genes were found in orthogroups? Answer this for all orthogroup comparions (many-to-many, one-to-one, etc).</summary>

```
#tps_in_OGs.sh
can_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_can_tps
chi_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_chi_tps

phylo_orthogroups=/core/labs/Wegrzyn/karl/tsuga_sel/orthofinder/complete_cds/OrthoFinder/Results_Aug19/Phylogenetic_Hierarchical_Orthogroups/N0.tsv

# Find the phylogenetic hierarchichal ortogroups
grep -f $can_tps $phylo_orthogroups > can_phylo_OGs
grep -f $chi_tps $phylo_orthogroups > chi_phylo_OGs
```

</details>

One one-to-one orthogroups between chinensis and canadensis were found:

```
N0.HOG0002156   OG0001134       ACN_02_TRINITY_DN9390_c0_g1_i1  ACH_02_TRINITY_DN5770_c0_g1_i25

```

#### OG0001134 

<details>
<summary>Create fasta files by orthogroup which contain the population of genes. Be sure to use the complete cds translated to proteins.</summary>

```
workdir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/

# The file containing the one-to-one TPS orthologs
tps_orthologs_1to1=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/one_to_one_tps_orthologs_tcan_tchi

# Take the locus file and use it with the cluster table to get a list of genes from the populaiton sets
## Specify the centroid table of parent-child relationships (uctable files)
uctable_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc
uctable_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc

# Reading the file line by line
while IFS=$'\t' read -r _ orthogroup_name can_locus chi_locus; do
    # Grep the content and save to a file named by orthogroup for canadensis
    grep "$can_locus" "$uctable_can" > "${orthogroup_name}_can.uc"
    
    # Grep the content and save to a file named by orthogroup for chinensis
    grep "$chi_locus" "$uctable_chi" > "${orthogroup_name}_chi.uc"
done < "$tps_orthologs_1to1"


# Remove the centroid records (removes duplicate information)
sed -i 's/^C.*$//g' *.uc
 
# Delete empty lines created after the sed
sed -i '/^$/d' *.uc 
 
# Path to the other script
scripts="/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/scripts"
 
# Loop through each file that matches the pattern *_cluster.uc
for OG_file in *.uc; do
     # Run the generate_grep_files.sh script with the OG_file as argument
     bash "$scripts/generate_grep_files.sh" "$OG_file"
 done
  
## Using seqkit, extract the desired protein sequences
# Protein files to grep from
complete_cds_pep_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep
complete_cds_pep_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep

# Function to grep for genes
# This will create empty files for seqkit grep combinations that don't exist. Don't worry about it for now.
grep_genes() {
    local species_file="$1"
    local species_suffix="$2"

    for grep_file in *_grep-locus.txt; do
        output_file="${grep_file%_grep-locus.txt}_tps_${species_suffix}.pep.fa"
        seqkit grep -r -f "$grep_file" "$species_file" > "$output_file"
    done
}

# grep for canadensis
grep_genes "$complete_cds_pep_can" "can"

# grep for chinensis
grep_genes "$complete_cds_pep_chi" "chi"

## Add TCan for canadensis (to help tracking)
sed -i 's/^>/>Tcan_/' *can.pep.fa
sed -i 's/^>/>Tchi_/' *chi.pep.fa

# Tidy up any empty pep files
for pep_file in *.pep.fa; do
    if [[ ! -s "$pep_file" ]]; then
        rm "$pep_file"
    fi
done

```

</details>

<details>
<summary>Cluster the OG protein fasta files. The goal here is to work with truly orthologs gene sets. OrthoFinder can be somewhat liberal in its attempt to find cluster and another cluster can help remove false positives from the workflow.</summary>

```
module load vsearch/2.22.1

for OG_pep in *pep.fa; do
    OG_name=$(echo "$OG_pep" | cut -d'_' -f1) # Extract the OG name part
    suffix=$(echo "$OG_pep" | grep -oE '_can|_chi') # Extract the _can or _chi part
    vsearch --cluster_fast "$OG_pep" --id 0.7 --centroids "${OG_name}_${suffix}_centroids.fa" --clusterout_id --clusterout_sort --threads 8 --uc "${OG_name}_${suffix}_terpene_uc" --otutabout "${OG_name}_${suffix}_terpene_otutable"
done

```

</details>

This step might be unnecessary, as clustering at 70% and 80% identitiy yielded one cluster for each species orthogroup. 

<details>
<summary>Combine the pep files by orthogroup.</summary>

```
cat OG0001134_ACN_02_TRINITY_DN9390_c0_g1_i1.p1_tps_can.pep.fa OG0001134_ACH_02_TRINITY_DN5770_c0_g1_i25.p1_tps_chi.pep.fa > OG0001134.pep.fa
```

</details>

#### OG0003764

This is a 1:many ortholog. Build population fasta files for it. 

<details>
<summary>Scratch for OG0003764</summary>

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/OG0003764

# Use the create_tps_by_OG.sh script.
cp ../OG0001134/create_tps_by_OG.sh .

#




```

</details>


### MAFFT

<details>
<summary>Simplify the headers, then run MAFFT on the proteins files.</summary>

```
sed -i 's/\s.*$//g' OG0001134.pep.fa 

module load mafft/7.471

mafft --maxiterate 1000 --genafpair OG0001134.pep.fa > OG0001134.pep.aln 

```

</details>

I also used mafft online. Which is very fast.

### Guidance2

Use Guidance to calculate alignment confidence scores. It's available oqline and works well there.

<details>
<summary>Use the python code to filter for individuals that have low guidance scores. Paste the remove list from guidance output, then use the python code to remove individuals.</summary>

```
# file with rm list: 

# remove inds
python3 filter_inds.py OG0001134.pep.fa remove_inds > OG0001134.N78.pep.fa


```

</details>

<details>
<summary>I'm going to start sending files to Guidance, PAl2NAL and Buster. Scratch below.</summary>

```
# Simplify the header names down to ten characters. It's not possible, I need the characters! PAML...arg
sed -i 's/\s.*$//' OG0001134_can_N33.pep.aln
sed -i 's/\s.*$//' OG0001134_chi_N53.pep.aln 

sed -i 's/\s.*$//' OG0001134_ACH_02_TRINITY_DN5770_c0_g1_i25.p1_tps_chi.pep.fa
sed -i 's/\s.*$//' OG0001134_ACN_02_TRINITY_DN9390_c0_g1_i1.p1_tps_can.pep.fa

# remove inds you want to rm (with vim)
# input: can_OG0001134.msa.pep.aln, output:can_OG0001134_IndFilter.msa.pep.aln 
# rm Tcan_Tca263_47Aa_TRINITY_DN10891_c0_g1_i2.p1
# input: chi_OG0001134.msa.pep.aln output: chi_OG0001134_IndFilter.msa.pep.aln
# rm:
# >Tchi_ACH_01_TRINITY_DN2518_c0_g1_i13.p1
# >Tchi_ACH_05_TRINITY_DN1116_c0_g1_i41.p1
# >Tchi_ACH_05_TRINITY_DN1116_c0_g1_i6.p1
# >Tchi_ACH_05_TRINITY_DN1116_c0_g1_i25.p1
# >Tchi_ACH_02_TRINITY_DN5770_c0_g1_i4.p1
# >Tchi_ACH_02_TRINITY_DN5770_c0_g1_i44.p1
# >Tchi_SRR12377235_TRINITY_DN6146_c0_g1_i1.p1
# >Tchi_ACH_02_TRINITY_DN5770_c0_g1_i36.p1
# >Tchi_ACH_02_TRINITY_DN5770_c0_g1_i26.p1
# >Tchi_ACH_12_TRINITY_DN6133_c0_g1_i5.p1
# >Tchi_ACH_05_TRINITY_DN1116_c0_g1_i39.p1
# >Tchi_SRR12377271_TRINITY_DN52441_c0_g1_i1.p1


# Get CDS file matching site/row filtered pep.aln
grep '>' can_OG0001134_IndFilter.msa.pep.aln > can_ind_filter
grep '>' chi_OG0001134_IndFilter.msa.pep.aln > chi_ind_filter

# Remove '>' and the Tcan_ for seqkit
sed -i 's/>//' *ind_filter
sed -i 's/Tcan_//' can_ind_filter 
sed -i 's/Tchi_//' chi_ind_filter 

# Grep with seqkit
seqkit grep -r -f can_ind_filter /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds > OG0001134_can_filtered.cds.fa
seqkit grep -r -f chi_ind_filter /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds > OG0001134_chi_filtered.cds.fa

# Simplify headers
sed -i 's/\s.*$//' OG0001134_c*_filtered.cds.fa

# Run pal2nal on xanadu
pal2nal.pl can_OG0001134_IndFilter.msa.pep.aln OG0001134_can_filtered.cds.fa -output fasta > OG0001134_can_filtered_codon.cds.fa 
pal2nal.pl chi_OG0001134_IndFilter.msa.pep.aln OG0001134_chi_filtered.cds.fa -output fasta > OG0001134_chi_filtered_codon.cds.fa 

# Go to http://www.datamonkey.org and use the selection method BUSTER-MH on the files
```

</details>

### ARCHIVE: Create Gene-wise TPS FASTA files

Working in the `tsuga_sel/multiple_seq_aln/create_fastas` directories, assemble the information you need.

<details>
<summary>Create fasta files for each TPS gene with create_tps_fasta.sh</summary>

```
#!/bin/bash
#SBATCH --job-name=Grep2Pop
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

workdir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/create_fastas

# Specify the centroid table of parent-child relationships
uctable_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc
uctable_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc

# Specify the locations of the unique lists of genes
unique_list_can_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_can_tps
unique_list_chi_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_chi_tps 

# Grep out the rows that contain TPS hits
grep -f $unique_list_can_tps $uctable_can > pop_set_tps_can.uc
grep -f $unique_list_chi_tps $uctable_chi > pop_set_tps_chi.uc

# Remove the centroid records (removes duplicate information)
sed -i 's/^C.*$//g' pop_set_tps_can.uc
sed -i 's/^C.*$//g' pop_set_tps_chi.uc

# Delete empty lines created after the sed
sed -i '/^$/d' pop_set_tps_can.uc 
sed -i '/^$/d' pop_set_tps_chi.uc 

# Take the column from each table containing the grep target
cut -f9 pop_set_tps_can.uc > genes2grep_can 
cut -f9 pop_set_tps_chi.uc > genes2grep_chi

# Remove the .p1 that is not in the original headers
sed -i 's/\.p[0-9]$//g' genes2grep_can
sed -i 's/\.p[0-9]$//g' genes2grep_chi

# Using seqkit, extract the desired sequences
combined_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/03_Assembly/assembly_min_contig_length_350/trinity_combine.fasta
combined_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/03_Assembly/trinity_combine.fasta

seqkit grep -r -f genes2grep_can $combined_can > combined_tps_can.fa 
seqkit grep -r -f genes2grep_chi $combined_chi > combined_tps_chi.fa 

# Create subdirectories for each species to prevent overwritting the files
for dir in chi_fasta can_fasta; do
  [ -d "$dir" ] && rm -r "$dir"
  mkdir "$dir"
done

cd can_fasta

# Split the cominbed fasta into a single fasta for each locus.
scripts=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/create_fastas/scripts
## Make 'grep files' for each locus
bash $scripts/generate_grep_files.sh $workdir/pop_set_tps_can.uc

# Remove the .p1 that is not in the original headers
sed -i 's/\.p[0-9]$//g' *_locus.txt

# Split the fasta into several files by locus
bash $scripts/split_fasta.sh  $workdir/combined_tps_can.fa 

# Add TCan for canadensis (to help tracking)
sed -i 's/^>/>Tcan_/' *fasta

# Repeat for chinensis
cd ../chi_fasta
bash $scripts/generate_grep_files.sh $workdir/pop_set_tps_chi.uc
sed -i 's/\.p[0-9]$//g' *_locus.txt
bash $scripts/split_fasta.sh  $workdir/combined_tps_chi.fa 
sed -i 's/^>/>Tchi_/' *fasta

```

</details>

<details>
<summary>The above code block relies on two scripts to create the locus files for seqkit to grep, and to split the combined fasta into a fasta for each locus.</summary>

```
# generate_grep_files.sh
#!/bin/bash

# Check if the pop_set file is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path_to_pop_set_file>"
    exit 1
fi

# Set the input file
pop_set_file="$1"

# Ensure the file exists
if [ ! -f "$pop_set_file" ]; then
    echo "Error: File '$pop_set_file' not found."
    exit 1
fi

# Initialize centroid variable
current_centroid=""

# Read the file line by line
while IFS=$'\t' read -r -a columns; do
    # Check the first column
    prefix="${columns[0]}"
    if [[ "$prefix" == "S" ]]; then
        current_centroid="${columns[8]}"
        echo "${current_centroid}" > "${current_centroid}_locus.txt"
    elif [[ "$prefix" == "H" && -n "$current_centroid" ]]; then
        target="${columns[8]}"
        echo "${target}" >> "${current_centroid}_locus.txt"
    fi
done < "$pop_set_file"


# split_fasta.sh
#!/bin/bash

# Check if the correct number of arguments is provided
if [ $# -ne 1 ]; then
  echo "Usage: $0 <path/to/input_fasta.fa>"
  exit 1
fi

# Path to the original FASTA file
FASTA_FILE="$1"

# Check if the provided FASTA file exists
if [ ! -f "$FASTA_FILE" ]; then
  echo "Error: FASTA file $FASTA_FILE not found."
  exit 1
fi

# Loop through the files with the "_locus.txt" suffix in the current directory
for HEADER_FILE in *_locus.txt; do
  # Extract the base name of the header file to use as the output FASTA file name
  BASENAME=$(basename "$HEADER_FILE" _locus.txt)
  OUTPUT_FASTA="${BASENAME}.fasta"
  
  # Use seqkit to extract sequences matching the headers in the current header file
  seqkit grep -r -f "$HEADER_FILE" "$FASTA_FILE" > "$OUTPUT_FASTA"
  
  echo "Extracted sequences for $BASENAME into $OUTPUT_FASTA"
done

echo "Separation complete."

```

</details>

After making the fasta files, there is a lot of variation of individuals containing each gene. Checkout `cat Grep2Pop_7209717*` for details.


### Create Background genome Alignments

Grap 50 genes with 1:1 orthology between my species. Make a separate alignment for each gene to use for HKA testing. This set is the null set. 


### Align with COATi

Use COATi to make alingments. I will make alignments at three levels: 
1. global alignment of TPS from both species,
2. OrthoGroup-wise alignment,
3. Gene-wise alignment.

I'm facing an issue of defining genes for the direct comparison of coding regions within a _gene_ between species. The HKA test is designed for gene comparisons, but I'm struggling to identify genes due to the evolutionary history and possibly technical issues. There is not good solution for identifying orthology in this scenario, as such, I'll calculate the HKA statistics on three levels and report them all. 

#### Global TPS alignment

To use COATi, I'll need the multi-sequence fasta, a tree of the sequences, and the name of the reference sequence. 

<details>
<summary>Use clsutalw to make a simple guide tree from a basic alingment.</summary>

```
module load clustalw/2.1
clustalw
# then follow the prompts to load the global_tps.fa and create a guide tree.
```

</details>

Download the guide tree and the `global_tps.fa` to local.

<details>
<summary>Use COATi to make a multiple-sequence alignment.</summary>

```
cd ~/Dropbox/tsuga/coati/global
coati msa global_tps.fa guide.tre "Tcan_ACN_01_TRINITY_DN11974_c0_g1_i1" \  
    -m mar-ecm \ # marginal empirical codon model using Gotoh's algorithm
    -o global_tps 

coati msa -m mar-ecm -o global_tps global_tps.fa global_tps.tre ref.fa

coati msa -m mar-ecm -o global_tps global_tps.fa global_tps.tre Tchi_ACH_03_TRINITY_DN5847_c0_g1_i3.fa

coati msa -m mar-ecm -o global_tps global_tps.fa global_tps.tre 'Tcan_ACN_01_TRINITY_DN11974_c0_g1_i1' 

```

</details>

I'm having issues with coati. I'll get a mafft run going for the global alingment

## Codon-aware sequence alignment tool (CAST)

Cast the nucleotide sequences according to an alignment of proteins.

```
from Bio import AlignIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment

def codon_aware_alignment(protein_alignment_file, nucleotide_fasta_file, output_file):
    protein_alignment = AlignIO.read(protein_alignment_file, "fasta")
    nucleotide_sequences = {rec.id: rec.seq for rec in AlignIO.read(nucleotide_fasta_file, "fasta")}

    aligned_nucleotide_records = []

    for protein_record in protein_alignment:
        aligned_nucleotide = ""
        nucleotide_sequence = nucleotide_sequences[protein_record.id]

        codon_pos = 0
        for aa in protein_record.seq:
            if aa == "-":
                aligned_nucleotide += "---"
            else:
                aligned_nucleotide += str(nucleotide_sequence[codon_pos:codon_pos + 3])
                codon_pos += 3

        aligned_nucleotide_records.append(SeqRecord(Seq(aligned_nucleotide), id=protein_record.id))

    aligned_nucleotide_alignment = MultipleSeqAlignment(aligned_nucleotide_records)
    AlignIO.write(aligned_nucleotide_alignment, output_file, "fasta")

protein_alignment_file = "protein_alignment.fasta"
nucleotide_fasta_file = "nucleotide_sequences.fasta"
output_file = "aligned_nucleotide_sequences.fasta"

codon_aware_alignment(protein_alignment_file, nucleotide_fasta_file, output_file)

```

Try it out on the `OG0001134` sequences with 78 inds. 

```
# pull out the cds of the inds you need
seqkit grep -r -f can_headers /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds > can_OG0001134.cds.fa
seqkit grep -r -f chi_headers /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds > chi_OG0001134.cds.fa

# Modify the headers to track species better
sed -i 's/^>/>Tcan_/g' can_OG0001134.cds.fa
sed -i 's/^>/>Tchi_/g' chi_OG0001134.cds.fa

# Combine the files
cat can_OG0001134.cds.fa chi_OG0001134.cds.fa > OG0001134.cds.fa

# Use cast
sbatch cast.sh
``` 




## Proof-of-Concept

I'm going to try to keep it simple and calculate the statistics for an easy locus. Find a locus that has one-to-one homology between species. Get the sequences in a fasta, then align them. Use pegas to calculate the number of segregating sites within species, and the number of fixed differences between species. This allows you to perform the MK test.

_Update_ I was able to get an alingmnet of protein sequences made. I'm running Guidance2 on it now to prune the alignment for low confidence sites. After pruning the alignment and you're happy with how it looks. Follow the beginner's guide and try to use CODEML for Ka/Ks. Then, after you are comfortable there, use pegas to get the info on segregating sites within, and fixed sites between for the MK test. 

What I'm not sure about is how to utilize the nucleotide sequence data. Several amino acids are coded by multiple codon sets, and I want that information present to caluclate the synonymous site statistics. How do I incorporate that information if I'm working iwth proteins? Read the beginner's guide top to bottom - hopefully there will be clues there.

## HMMER > MSA

I'm going to try to make alingments from the HMMER results themselves. Hopefully I'll get better representation.

### Collect reference cds for clustering

Use `collect_ref_tps.sh` to generate a vsearch of the reference cds for identifying orthologous genes.

<details>
<summary>collect_ref_tps.sh</summary>

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result

# Collect tps results
uniq_can=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/unique_list_can_tps
uniq_chi=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/unique_list_chi_tps

# Grab TPS seqs from the reference to cluster for ortholog detection
can_ref=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds.fa
chi_ref=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds.fa

seqkit grep -r -f $uniq_can $can_ref > ref_can_tps_21loci.cds.fa_tmp
seqkit grep -r -f $uniq_chi $chi_ref > ref_chi_tps_21loci.cds.fa

# Count how many times each type of transcript is observed
grep -o 'type:[^ ]*' *loci.cds.fa* | sort | uniq -c
grep -o 'type:[^ ]*' *loci.cds.fa* 

# Remove the internal sequence from the canadensis ref
seqkit grep -v -r -p "ACN_01_TRINITY_DN36345_c0_g1_i1.p1" ref_can_tps_21loci.cds.fa_tmp > ref_can_tps_20loci.cds.fa

# Merge the reference cds files and cluster them
sed -i 's/\s.*$//' ref_*_tps_2*loci.cds.fa
sed -i 's/^>/>Tchi_/' ref_chi_tps_21loci.cds.fa
sed -i 's/^>/>Tcan_/' ref_can_tps_20loci.cds.fa
sed -i 's/_frame=1//' ref_*_tps_2*loci.cds.fa
cat ref_can_tps_20loci.cds.fa ref_chi_tps_21loci.cds.fa > ref_TPS_can_chi.cds.fa

# vsearch
module load vsearch/2.22.1
vsearch --cluster_fast ref_TPS_can_chi.cds.fa --id 0.8 --clusterout_id --clusterout_sort --threads 8 --uc ref_TPS_uc

#END

```

</details>

```
cd /core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result

# Collect resources
cp /core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/can_tps_3domains.hmmscan.out .
cp /core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/chi_tps_3domains.hmmscan.out .

# In Vim make a file with unique hits of both files. Keep the '.pN' this time for more specificity.
```

Here's a look at the completeness of the TPS transcripts in both reference files.

| Set | Type | Count |  
| ref_can_tps | complete |        16 | 
| ref_can_tps | 5prime_partial |   4 | 
| ref_can_tps | internal |         1 | 
| ref_chi_tps | complete |        10 | 
| ref_chi_tps | 5prime_partial |   6 | 
| ref_chi_tps | 3prime_partial |   5 | 

Drop the 1 internal sequence, it will be hard to believe the codon alignments for that one. It's `ACN_01_TRINITY_DN36345_c0_g1_i1.p1`.

### Create pep.fa for population sets

Create a fasta for each ortholog for each species.


<details>
<summary>Generate the grep files from the cureated list of loci to move forward to the next step.</summary> 


```
python ../scripts/generate_grep_files.py locus_list_can /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc 
#python ../scripts/generate_grep_files.py locus_list_chi /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc

```

</details>

<details>
<summary>For some odd reason the `generate_grep_files.py` creates a _ton_ of files for the chinensis grep. Do it manually with `grep_chinensis.sh`</summary>

```
#tidy up the files and keep only the 9th column
sed -i 's/^\([^\t]*\t\)\{8\}\([^\t]*\)\t.*/\2/' *grep-locus.txt

# Somehow a lot of duplicates were added to the grep-loucs files, rm them
# unique_sort.sh
#!/bin/bash

for file in *grep-locus.txt; do
  sort -u "$file" -o "$file"
done
```

</details>

Check the number of sequences in each grep file.

```
    87 ACH_02_TRINITY_DN5770_c0_g1_i25.p1_grep-locus.txt
   55 ACH_01_TRINITY_DN6523_c0_g1_i8.p1_grep-locus.txt
   41 ACH_05_TRINITY_DN20794_c0_g1_i2.p1_grep-locus.txt
   27 ACH_01_TRINITY_DN5168_c0_g1_i7.p1_grep-locus.txt
   26 ACH_01_TRINITY_DN11517_c0_g1_i3.p1_grep-locus.txt
   23 ACH_02_TRINITY_DN5461_c0_g2_i1.p1_grep-locus.txt
   22 ACH_03_TRINITY_DN8469_c0_g1_i1.p1_grep-locus.txt
   17 ACH_03_TRINITY_DN33076_c0_g1_i1.p1_grep-locus.txt
   15 JSP_01_TRINITY_DN4682_c0_g1_i6.p1_grep-locus.txt
   10 JSP_01_TRINITY_DN4682_c0_g4_i1.p1_grep-locus.txt
   10 ACH_02_TRINITY_DN3307_c1_g1_i5.p1_grep-locus.txt
    9 ACN_02_TRINITY_DN8790_c0_g1_i3.p1_grep-locus.txt
    9 ACN_02_TRINITY_DN8582_c0_g1_i1.p1_grep-locus.txt
    6 ACN_02_TRINITY_DN176_c0_g1_i4.p1_grep-locus.txt
    6 ACN_01_TRINITY_DN38968_c0_g1_i1.p1_grep-locus.txt
    6 ACH_03_TRINITY_DN8327_c0_g1_i2.p1_grep-locus.txt
    5 JSP_01_TRINITY_DN5141_c0_g1_i9.p1_grep-locus.txt
    5 ACN_02_TRINITY_DN9390_c0_g1_i1.p1_grep-locus.txt
    5 ACH_02_TRINITY_DN3307_c0_g1_i2.p1_grep-locus.txt
    4 ACN_02_TRINITY_DN10530_c0_g1_i7.p1_grep-locus.txt
    4 ACH_01_TRINITY_DN41030_c0_g1_i1.p1_grep-locus.txt
    3 ACN_02_TRINITY_DN176_c0_g1_i8.p1_grep-locus.txt
    3 ACH_02_TRINITY_DN3307_c0_g2_i1.p1_grep-locus.txt
    2 JSP_02_TRINITY_DN3218_c0_g2_i2.p1_grep-locus.txt
    2 ACN_02_TRINITY_DN1779_c0_g1_i4.p1_grep-locus.txt
    2 ACN_01_TRINITY_DN5904_c0_g1_i2.p3_grep-locus.txt
    1 JSP_02_TRINITY_DN2883_c0_g1_i14.p1_grep-locus.txt
    1 JSP_01_TRINITY_DN4682_c0_g5_i1.p1_grep-locus.txt
    1 ACN_02_TRINITY_DN7895_c0_g1_i2.p1_grep-locus.txt
    1 ACN_02_TRINITY_DN7895_c0_g1_i1.p1_grep-locus.txt
    1 ACN_01_TRINITY_DN8033_c0_g1_i2.p1_grep-locus.txt
    1 ACN_01_TRINITY_DN187_c0_g1_i28.p1_grep-locus.txt
    1 ACH_05_TRINITY_DN15765_c0_g1_i1.p1_grep-locus.txt
    1 ACH_04_TRINITY_DN7114_c0_g1_i5.p1_grep-locus.txt
    1 ACH_04_TRINITY_DN22290_c0_g1_i1.p2_grep-locus.txt
    1 ACH_03_TRINITY_DN8327_c0_g2_i5.p1_grep-locus.txt
    1 ACH_03_TRINITY_DN5770_c1_g1_i3.p1_grep-locus.txt
```

<details>
<summary>Use seqkit grep to create the fasta files of peptide sequences</summary>

```
# Make a file to grep from 
ls *grep-locus.txt > make_fasta_list

```

</details>



<details>
<summary>Follow up on some seqkit greps</summary>

```
#JSP_01 DN4682
seqkit grep -r -f JSP_01_TRINITY_DN4682_c0_g1_i6.p1_grep-locus.txt /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep > JSP_01_DN4682_c0_g1_i6.pep.fasta


# ACH_02_TRINITY_DN3307_c0_g1_i3 is missing
# Make grep-locus file. Only 1 seq associated
seqkit grep -r -p "ACH_02_TRINITY_DN3307_c0_g1_i3.p1" /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep > ACH_02_DN3307_c0_g1_i3.p1.pep.fa

# Clean up
sed

# ACH_04_DN9055
grep 'ACH_04_TRINITY_DN9055_c0_g1_i4.p1' /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc > ACH_04_TRINITY_DN9055_c0_g1_i4.p1_grep-locus.txt
seqkit grep -r -f ACH_04_TRINITY_DN9055_c0_g1_i4.p1_grep-locus.txt /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep > SG01_ACH_04_DN9055_c0_g1_i4.p1.pep.fa

#SG06 ACH_01_TRINITY_DN41030_c0_g1_i1.p1	
#SG07 ACN_02_TRINITY_DN22506_c0_g2_i1.p1	
#SG08 ACH_02_TRINITY_DN16265_c0_g1_i2.p1	
grep 'ACH_01_TRINITY_DN41030_c0_g1_i1.p1' /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc > ACH_01_TRINITY_DN41030_c0_g1_i1.p1_grep-locus.txt
#only 1 sequence here. ignore: grep 'ACH_02_TRINITY_DN16265_c0_g1_i2.p1' /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc > ACH_02_TRINITY_DN16265_c0_g1_i2.p1_grep-locus.txt
#only 1 sequence here. ignore: grep 'ACN_02_TRINITY_DN22506_c0_g2_i1.p1' /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc > ACN_02_TRINITY_DN22506_c0_g2_i1.p1_grep-locus.txt



```

</details>

Using the ortholog table, assign orthogroups to sequences. 

The cluster at 80% is yielding a lot of groups that are heavily biased towards one species or another. In other words, I have a lot of OG's with 1 canadensis and 80 chinensis. Try lowering the cluster similarity to 70, and 

```
module load vsearch/2.22.1
vsearch --cluster_fast ref_TPS_can_chi.cds.fa --id 0.7 --clusterout_id --clusterout_sort --threads 8 --uc ref_TPS_70_uc

# Output
Reading file ref_TPS_can_chi.cds.fa 100%  
57597 nt in 42 seqs, min 309, max 3162, avg 1371
Masking 100% 
Sorting by length 100%
Counting k-mers 100% 
Clustering 100%  
Sorting clusters 100%
Writing clusters 100% 
Clusters: 16 Size min 1, max 13, avg 2.6
Singletons: 9, 21.4% of seqs, 56.2% of clusters


```


<details>
<summary>Now, read the og tables into R and merge the sequence count info into the new tab70. The goal is to get an understanding for the balance of the sequence data sets.</summary>

```
library(tidyverse)
tab70 = read.table("ortholog_table_70.uc",T,'\t')
tab80=read.table("ortholog80_seq_count.txt",T,"\t")

# Add a new column to tab70 and tab80 to mark their origin
tab70 <- tab70 %>%
  mutate(Source = "tab70")

tab80 <- tab80 %>%
  mutate(Source = "tab80") %>%
  rename(Name = RefSeqName)

# Perform a join merge
merged_data <- tab70 %>%
  left_join(tab80, by = "Name") 

# Send just the info for cluster70 to an object
out = merged_data %>% select(OG.x, Record, SeqCount, Name, Parent, PercSimilarity, Type, Source.x)

```

</details>


<details>
<summary>Next, use the renamer.sh script to rename the file names by their orthogroup.</summary>

```
#renamer.sh
#!/bin/bash

# Check if the input file is provided
if [ $# -ne 1 ]; then
  echo "Usage: $0 <input_file>"
  exit 1
fi

input_file="$1"

# Check if the input file exists
if [ ! -f "$input_file" ]; then
  echo "File not found: $input_file"
  exit 1
fi

# Read the input file line by line
while read -r og file_name; do
  # Check if the old file exists
  if [ -f "$file_name" ]; then
    # Construct the new filename
    new_file="${og}_${file_name}"
    # Rename the file
    mv "$file_name" "$new_file"
    echo "Renamed $file_name to $new_file"
  else
    echo "File not found: $file_name, skipping..."
  fi
done < "$input_file"


```

</details>


<details>
<summary>Finally, combine OGs.fa's into an OG for each species. Then align those.</summary>

```
#copy_fastas.sh
#!/bin/bash
cat OG02_ACH_02_DN5461_c0_g2_i1.p1.pep.fa OG02_ACH_03_DN5770_c1_g1_i3.p1.pep.fa > OG02_chi.popfile.pep.fa
cat OG02_ACN_02_DN176_c0_g1_i4.p1.pep.fa OG02_ACN_02_DN176_c0_g1_i8.p1.pep.fa OG02_ACN_02_DN8582_c0_g1_i1.p1.pep.fa OG02_JSP_01_DN5141_c0_g1_i9.p1.pep.fa OG02_JSP_02_DN2883_c0_g1_i14.p1.pep.fa > OG02_can.popfile.pep.fa
cat OG03_ACH_03_DN8469_c0_g1_i1.p1.pep.fa OG03_ACH_04_DN22290_c0_g1_i1.p2.pep.fa > OG03_chi.popfile.pep.fa 
cat OG04_ACH_01_DN11517_c0_g1_i3.p1.pep.fa OG04_ACH_04_DN7114_c0_g1_i5.p1.pep.fa > OG04_chi.popfile.pep.fa 
cat OG05_ACN_01_DN5904_c0_g1_i2.p3.pep.fa OG05_ACN_02_DN10530_c0_g1_i7.p1.pep.fa OG05_ACN_02_DN8790_c0_g1_i3.p1.pep.fa > OG05_can.popfile.pep.fa
cat OG06_ACH_01_DN5168_c0_g1_i7.p1.pep.fa OG06_ACH_02_DN3307_c0_g1_i2.p1.pep.fa OG06_ACH_02_DN3307_c1_g1_i5.p1.pep.fa OG06_ACH_03_DN8327_c0_g1_i2.p1.pep.fa OG06_ACH_05_DN15765_c0_g1_i1.p1.pep.fa OG06_ACH_05_DN20794_c0_g1_i2.p1.pep.fa > OG06_chi.popfile.pep.fa
cat OG06_ACN_01_DN187_c0_g1_i28.p1.pep.fa OG06_ACN_02_DN7895_c0_g1_i1.p1.pep.fa OG06_ACN_02_DN7895_c0_g1_i2.p1.pep.fa OG06_JSP_01_DN4682_c0_g1_i6.p1.pep.fa OG06_JSP_01_DN4682_c0_g4_i1.p1.pep.fa OG06_JSP_01_DN4682_c0_g5_i1.p1.pep.fa OG06_JSP_02_DN3218_c0_g2_i2.p1.pep.fa > OG06_can.popfile.pep.fa
cat OG07_ACH_02_DN3307_c0_g1_i3.p1.pep.fa OG07_ACH_02_DN3307_c0_g2_i1.p1.pep.fa OG07_ACH_03_DN8327_c0_g2_i5.p1.pep.fa > OG07_chi.popfile.pep.fa 
```
</details>

Afterwards, concatenate the OG's by species into a super fasta for each OG. Then, count and remove the number of internal sequences in each file.


<details>
<summary>Count the number of internal sequences</summary>

```
grep -c "internal" 

```

</details>

Don't worry about the internal sequences for now. Align and use guidance to do the scoring

### MAFFT

<details>
<summary>Align the OG's by species using mafft</summary>

```
#!/bin/bash

module load mafft/7.471

pep_fa=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/ready2align_70/*pep.fa
outdir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/mafft_alignments

for file in $pep_fa; do
  base_name=$(basename $file .pep.fa)
  mafft --maxiterate 1000 --genafpair $file > "$outdir/$base_name.aln"
done

```

</details>

I ended up using mafft alignments from geneious. I filtered inds based on the visual quality of the alignments, and realigned filtered alignments.

#### Remove stop codons


<details>
<summary>The busted analysis won't work with stop codons, replace them in the protein alignments with a dash</summary>

```
sed -i 's/*/-/' *pep.aln

```

</details>

### CDS

<detials>
<summary>Grab CDS from the remaining individuals in the geneious mafft alignments.</summary>

```
for file in *.pep*; do grep '^>' "$file" | sed 's/>//' > "${file}.headers"; done

```

</detials>

<detials>
<summary>Use the headers to grab cds.</summary>

```


```

</detials>

<detials>
<summary>Simplify the headers.</summary>

```
sed -i 's/\s.*$//' *cds.fa

```

</detials>

### PAL2NAL

<detials>
<summary>Use PAL2NAL to get the cds in codon frame alignment.</summary>

```

pep_dir="/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_mafft_aln"
cds_dir="/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_cds_set"
output_dir="/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_pal2nal"

mkdir -p $output_dir

cd $pep_dir

for pep_file in *.pep.aln; do
  base_name="${pep_file%%.pep.aln}"
  cds_file="$cds_dir/${base_name}.cds.fa"
  reordered_cds_file="$cds_dir/${base_name}.reordered.cds.fa"
  output_file="$output_dir/${base_name}.aligned.cds.fa"

  if [ -f "$cds_file" ]; then
    # Extract IDs from pep file
    grep '>' $pep_file | sed 's/>//' > temp_ids.txt

    # Reorder CDS based on the order of PEP IDs
    > $reordered_cds_file
    while IFS= read -r id; do
      grep -A 1 ">$id" $cds_file >> $reordered_cds_file
    done < temp_ids.txt

    rm temp_ids.txt

    # Run pal2nal
    pal2nal.pl $pep_file $reordered_cds_file -nomismatch -output fasta > $output_file

    echo "Processed $pep_file and $cds_file"
  else
    echo "CDS file not found for $pep_file"
  fi
done

```

</detials>



OG1B didn't pal2nal for some reason. And I had to do it manually on the pal2nal website.

### Hyphy

Run hyphy to get the dN/dS estimates.

```
module load hyphy/2.5.38



```



### Hyphy online

Use the online tools for datamonkey to do the jobs. 

<detials>
<summary>Keep track of the runs here.</summary>

| JobID | dN/dS | url |
| :--- | :--- | :--- |
| | | | 
| | | | 
| | | | 

Unconstrained model	−2026.89	4132.49	39	Tested ω
0.02198 (82.386%) 0.1592 (17.614%) 1.002 (0.0000%)
Mean = 0.04615, CoV = 1.132



</details>

## OrthoFinder Results

Scarcth for makign OrthoFinder orthogroup files for dN/dS analysis

### Get popfiles

#### OG0001134b

```
grep -e 'ACN_02_TRINITY_DN1779_c0_g1_i4.p1' /core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc > ACN_02_TRINITY_DN1779_c0_g1_i4.p1_cluster.uc 


```



## Phylogenetic Comparison 

#### Online tools

Using an online tool, make the protein alignment and the tree. Plot the tree with FigTree

#### IQTREE

Using IQTREE or other methods build a phylogeny that is well-supported.


## Variant Calling

### BWA alignments

<details>
<summary>Use bwa-mem to make alignments from the fastq files.</summary>

```



```

</details>

## Multi-species Comparative Analyses

I can see a broad OrthoFinder or comparative selection test as useful. I'm looking at the population level for sequence evolution, I could easily increase the scale and see if patterns emerge at a larger scale that explain the pattern of susceptibility in the eastern hemlock. 

One potential path is to grab all of the hemlock transcriptomes you can find, plus genome sequences (filtered to cds and/or protein) for a good diversity panel. The panel could include conifers, angiosperms, ferns, lycophytes, etc. Do an OF analysis for gene family expansion (CAFE). Using the same gene trees, do the Likelihood ratio test chatGPT gave you, or some other comparative natural selection statistic. The goal here is to understand better the patterns within Tsuga. Be careful how you interpret the results. Without having done this type of analysis before, I may want to return to only using Tsuga-Nothotsuga and stay in the pinaceae. I probably don't even need angiosperms.

_grab a bunch of conifers to get good gene family evolution numbers there_

The OrthoFinder analysis with multiple species is a usefull tool for investigating gene family evolution, with the caveat that transcriptomes suffer from incompleteness. Including some with well-documented genomes and TPS enzymes? I could include a good conifer genome (giant sequoia), arabidopsis, populus, j. cinerea, and my two chinensis, along with a few other well documented genomes that have TPS written about them. Since OF uses a comparative phylogentic approach, the input should have all of the species I want to work with (me thinks). Thus, a good sampling of outgroups in addition to my two ingroups, and even the other tsuga species, would be a good idea (I think). THis approach is more about finding good TPS enzymes_

Or, _you can look at what's expanding and contracting in just Tsuga. This approach is more about finding out what's going on in hemlocks._

Is there a way to combin the two? Not sure that's possible since the CAFE analysis is specific to the input data. It's ok to have two OF runs, one to find protein models and the other to look at hemlocks. Or, you can use the HMMER to find your protein models in Chi/Can, then use a hemlock-specific analysis to look at what's expanding or contracting.

Decision: First, stick to your questions and find the TPS protein models. Then return to doing a comparative analysis and possible getting more terpenoid data. 

A nice comparative approach could be:

<details>
<summary>According to chatGPT, this could be a strategy for a multi-species selection test. See if there are any better methods from the literature if you want to pursue this more.</summary>

    To identify positive, balancing, negative, or neutral selection on genes across multiple species in a phylogeny, one commonly used approach is to perform a codon-based likelihood ratio test (LRT) using maximum likelihood methods. The LRT compares the fit of different models of codon evolution to determine which model best explains the observed sequence data.

    One popular method for conducting such tests is the likelihood ratio test implemented in the program CODEML, which is part of the PAML (Phylogenetic Analysis by Maximum Likelihood) software package. CODEML allows you to compare different models, including those that incorporate positive selection, purifying selection, and neutral evolution.

    Here's a general outline of the steps involved in using CODEML for selection analysis:

    Prepare your sequence data: Align the protein-coding sequences of the genes of interest across multiple species. Ensure that the alignment is accurate and free from errors.
    Construct a phylogenetic tree: Build a phylogenetic tree representing the evolutionary relationships among the species included in your analysis. You can use phylogenetic inference methods such as maximum likelihood or Bayesian inference.
    Select appropriate models: Determine which models to compare based on the hypothesis you want to test. For example, to detect positive selection, you might compare the M7 (neutral) and M8 (positive selection) models.
    Run the likelihood ratio test: Use CODEML to perform the likelihood ratio test by specifying the appropriate models and providing the aligned sequences and phylogenetic tree as input. CODEML will calculate the likelihood scores under different models and compare them using the likelihood ratio test statistic.
    Interpret the results: Evaluate the likelihood ratio test statistic to determine whether there is evidence for positive, balancing, negative, or neutral selection. The significance of the test can be assessed using the chi-squared distribution with the degrees of freedom equal to the difference in the number of parameters between the models.
    It's important to note that selection analysis is a complex and specialized field, and additional considerations and alternative methods may be appropriate depending on the specifics of your study. Consulting the relevant literature, seeking guidance from experts, or utilizing specialized software or packages specific to your research question can provide further insights and resources for conducting selection analysis in a multi-species phylogenetic context. 
</details>

Look through your Evolution-2023 notes for inspiration. Ask around too (After HKA is done).

## To Do List

Once I'm happy with reference transcriptomes, what are the next steps?

I need to:
- Create transcript IDs that are common across species (not sure how to do this)
- Phase sequences
- Identify codon position for each nucleotide within a transcript
- Perform the pop. gen statistics




SeqinR may be the program I want to use for the MK test. At least take a look at it: https://cran.r-project.org/web/packages/seqinr/index.html



