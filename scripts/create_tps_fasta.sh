#!/bin/bash
#SBATCH --job-name=Grep2Pop
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

workdir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/create_fastas

# Specify the centroid table of parent-child relationships
uctable_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc
uctable_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc

# Specify the locations of the unique lists of genes
unique_list_can_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_can_tps
unique_list_chi_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_chi_tps 

# Grep out the rows that contain TPS hits
grep -f $unique_list_can_tps $uctable_can > pop_set_tps_can.uc
grep -f $unique_list_chi_tps $uctable_chi > pop_set_tps_chi.uc

# Remove the centroid records (removes duplicate information)
sed -i 's/^C.*$//g' pop_set_tps_can.uc
sed -i 's/^C.*$//g' pop_set_tps_chi.uc

# Delete empty lines created after the sed
sed -i '/^$/d' pop_set_tps_can.uc 
sed -i '/^$/d' pop_set_tps_chi.uc 

# Take the column from each table containing the grep target
cut -f9 pop_set_tps_can.uc > genes2grep_can 
cut -f9 pop_set_tps_chi.uc > genes2grep_chi

# Remove the .p1 that is not in the original headers
sed -i 's/\.p[0-9]$//g' genes2grep_can
sed -i 's/\.p[0-9]$//g' genes2grep_chi

# Using seqkit, extract the desired sequences
combined_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/03_Assembly/assembly_min_contig_length_350/trinity_combine.fasta
combined_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/03_Assembly/trinity_combine.fasta

seqkit grep -r -f genes2grep_can $combined_can > combined_tps_can.fa 
seqkit grep -r -f genes2grep_chi $combined_chi > combined_tps_chi.fa 

# Create subdirectories for each species to prevent overwritting the files
for dir in chi_fasta can_fasta; do
  [ -d "$dir" ] && rm -r "$dir"
  mkdir "$dir"
done

cd can_fasta

# Split the cominbed fasta into a single fasta for each locus.
scripts=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/create_fastas/scripts
## Make 'grep files' for each locus
bash $scripts/generate_grep_files.sh $workdir/pop_set_tps_can.uc

# Remove the .p1 that is not in the original headers
sed -i 's/\.p[0-9]$//g' *_locus.txt

# Split the fasta into several files by locus
bash $scripts/split_fasta.sh  $workdir/combined_tps_can.fa 

# Add TCan for canadensis (to help tracking)
sed -i 's/^>/>Tcan_/' *fasta

# Repeat for chinensis
cd ../chi_fasta
bash $scripts/generate_grep_files.sh $workdir/pop_set_tps_chi.uc
sed -i 's/\.p[0-9]$//g' *_locus.txt
bash $scripts/split_fasta.sh  $workdir/combined_tps_chi.fa 
sed -i 's/^>/>Tchi_/' *fasta
