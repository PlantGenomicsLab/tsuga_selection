#!/bin/bash
#SBATCH --job-name=fasta_cat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

arabi=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/arabidopsis/arabidopsis_thaliana_tps.pep.fa
conifer=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/conifer/picea_sp_tps_newHeaders.pep.fa
physco=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/physcomitrium/physcomitrium_tps.pep.fa
populus=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/populus/populus_tps_ncbi.pep.fa
tomato=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tomato/Slycopersicum_tps.pep.fa
tchi=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tsuga/chi_tps.pep.fa
tcan=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tsuga/can_tps.pep.fa 

cat $arabi $conifer $physco $populus $tomato $tchi $tcan > tps_7species.pep.fa 

# Remove the '*' that are there for some reason
sed -i 's/\*//' tps_7species.pep.fa

# Simplify the headers
sed -i 's/\s.*$//' tps_7species.pep.fa

# Set wrap to 80 characters
seqkit seq -w 80 <(cat tps_7species.pep.fa) > tps_7species.pep.fa
