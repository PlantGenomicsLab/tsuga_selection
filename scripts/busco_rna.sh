#!/bin/bash
#SBATCH --job-name=CanBUSCOrna
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --qos=general
#SBATCH --partition=xeon
#SBATCH --mail-type=END
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH --mem=10G
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/5.0.0

transcriptome=/core/labs/Wegrzyn/karl/tsuga_sel/data/canadensis/assembly/05_Clustering/centroids.fasta
#odb=embryophyta_odb10
odb=/isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10
busco -f -i $transcriptome -l $odb -o busco_rna -m tran --offline
