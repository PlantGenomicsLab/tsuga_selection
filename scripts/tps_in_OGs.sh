#!/bin/bash
#SBATCH --job-name=tps_in_OGs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

can_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_can_tps
chi_tps=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/hmmer/unique_list_chi_tps

phylo_orthogroups=/core/labs/Wegrzyn/karl/tsuga_sel/orthofinder/complete_cds/OrthoFinder/Results_Aug18/Phylogenetic_Hierarchical_Orthogroups/N0.tsv

# Find the phylogenetic hierarchichal ortogroups
grep -f $can_tps $phylo_orthogroups > can_phylo_OGs
grep -f $chi_tps $phylo_orthogroups > chi_phylo_OGs
