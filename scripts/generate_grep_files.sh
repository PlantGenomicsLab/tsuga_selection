#!/bin/bash

# Check if the OG_uc_file is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path_to_OG_uc_file>"
    exit 1
fi

# Set the input file
OG_uc_file="$1"

# Ensure the file exists
if [ ! -f "$OG_uc_file" ]; then
    echo "Error: File '$OG_uc_file' not found."
    exit 1
fi

# Extract the orthogroup name from the file name
orthogroup_name=$(basename "$OG_uc_file" | cut -f 1 -d '_')

# Initialize centroid variable
current_centroid=""

# Read the file line by line
while IFS=$'\t' read -r -a columns; do
    # Check the first column
    prefix="${columns[0]}"
    if [[ "$prefix" == "S" ]]; then
        current_centroid="${columns[8]}"
        echo "${current_centroid}" > "${orthogroup_name}_${current_centroid}_grep-locus.txt"
    elif [[ "$prefix" == "H" && -n "$current_centroid" ]]; then
        target="${columns[8]}"
        echo "${target}" >> "${orthogroup_name}_${current_centroid}_grep-locus.txt"
    fi
done < "$OG_uc_file"
