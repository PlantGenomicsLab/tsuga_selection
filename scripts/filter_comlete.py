import sys
from Bio import SeqIO
import os

def filter_complete_sequences(input_file, output_file):
    with open(output_file, 'w') as out_handle:
        for record in SeqIO.parse(input_file, 'fasta'):
            if 'complete' in record.description:
                SeqIO.write(record, out_handle, 'fasta')

def main():
    if len(sys.argv) != 2:
        print("Usage: python filter_complete.py /path/to/fasta.pep")
        sys.exit(1)

    input_file = sys.argv[1]
    base_name, extension = os.path.splitext(os.path.basename(input_file))
    output_file = os.path.join(os.getcwd(), base_name + '_complete' + extension)

    filter_complete_sequences(input_file, output_file)
    print(f"Complete sequences have been saved to {output_file}")

if __name__ == "__main__":
    main()

