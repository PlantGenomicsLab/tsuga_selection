#!/bin/bash
#SBATCH --job-name=CanTrinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=200G
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --array=[0-19]
##SBATCH --mail-type=ALL
##SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

hostname
date

module load trinity/2.14.0
module load samtools

# this is an array job. SLURM will run this script 20 times in parallel (#SBATCH --array=[0-19]) contingent on resource availability
	# each time SLURM will change the value of the variable SLURM_ARRAY_TASK_ID
	# we'll use a bash array and that variable to retrieve a diferent sample

# a bash array containing the sample IDs
LIST=($(echo ACN_01 ACN_02 JSP_01 JSP_02 JSP_03 SRR12377238 SRR12377239 SRR12377240 SRR12377241 SRR6134124 SRR6134132 SRR6134201 SRR6134204 SRR6134205 TSP_01 TSP_02 TSP_03 Tca2187_98Aa Tca227_85Ab Tca263_47Aa))

# get one sample ID using the slurm array task ID
SAM=${LIST[$SLURM_ARRAY_TASK_ID]}

Trinity --seqType fq \
	--left ../../02_Quality_Control/trim_${SAM}_1.fastq.gz \
	--right ../../02_Quality_Control/trim_${SAM}_2.fastq.gz \
	--min_contig_length 350 \
	--CPU 36 \
	--max_memory 200G \
	--output trinity_${SAM} \
	--full_cleanup 
date 
