#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16 
#SBATCH --mem=75G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

##################################################
##  	Clustering using vsearch		##
##################################################
module load vsearch/2.22.1

sequences=/core/labs/Wegrzyn/karl/tsuga_sel/phylogeny/tps/tps_7species.cds.fa

vsearch --cluster_fast $sequences --id 0.45 --centroids terpene_centroids_id45.fa --clusterout_sort --clusterout_id --log LOGFile45 --threads 16 --biomout terpene_7sp_id45.biomout --uc terpene_uc_7sp_id45 --otutabout terpene_7sp_id45_otutable

vsearch --cluster_fast $sequences --id 0.5 --centroids terpene_centroids_id50.fa --clusterout_sort --clusterout_id --log LOGFile50 --threads 16 --biomout terpene_7sp_id50.biomout --uc terpene_uc_7sp_id50 --otutabout terpene_7sp_id50_otutable

vsearch --cluster_fast $sequences --id 0.55 --centroids terpene_centroids_id55.fa --clusterout_sort --clusterout_id --log LOGFile55 --threads 16 --biomout terpene_7sp_id55.biomout --uc terpene_uc_7sp_id55 --otutabout terpene_7sp_id55_otutable

vsearch --cluster_fast $sequences --id 0.6 --centroids terpene_centroids_id60.fa --clusterout_sort --clusterout_id --log LOGFile60 --threads 16 --biomout terpene_7sp_id60.biomout --uc terpene_uc_7sp_id60 --otutabout terpene_7sp_id60_otutable

vsearch --cluster_fast $sequences --id 0.7 --centroids terpene_centroids_id70.fa --clusterout_sort --clusterout_id --log LOGFile70 --threads 16 --biomout terpene_7sp_id70.biomout --uc terpene_uc_7sp_id70 --otutabout terpene_7sp_id70_otutable

vsearch --cluster_fast $sequences --id 0.75 --centroids terpene_centroids_id75.fa --clusterout_sort --clusterout_id --log LOGFile75 --threads 16 --biomout terpene_7sp_id75.biomout --uc terpene_uc_75sp_id70 --otutabout terpene_7sp_id75_otutable

vsearch --cluster_fast $sequences --id 0.8 --centroids terpene_centroids_id80.fa --clusterout_sort --clusterout_id --log LOGFile80 --threads 16 --biomout terpene_7sp_id80.biomout --uc terpene_uc_7sp_id80 --otutabout terpene_7sp_id80_otutable

vsearch --cluster_fast $sequences --id 0.85 --centroids terpene_centroids_id85.fa --clusterout_sort --clusterout_id --log LOGFile85 --threads 16 --biomout terpene_7sp_id85.biomout --uc terpene_uc_7sp_id85 --otutabout terpene_7sp_id85_otutable

vsearch --cluster_fast $sequences --id 0.9 --centroids terpene_centroids_id90.fa --clusterout_sort --clusterout_id --log LOGFile90 --threads 16 --biomout terpene_7sp_id90.biomout --uc terpene_uc_7sp_id90 --otutabout terpene_7sp_id90_otutable

date
