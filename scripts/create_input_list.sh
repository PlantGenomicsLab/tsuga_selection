#!/bin/bash
#SBATCH --job-name=create
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Array of file names
files=("can_p450.out.txt" "chi_terpene.out.txt" "can_terpene.out.txt" "chi_p450.out.txt")

# Print the header names to the output file
echo -e "species\tprotein_name\ttranscript" > output.txt

# Iterate over each file
for file in "${files[@]}"; do
    # Extract the base name without the extension
    base_name=$(basename "$file" .out.txt)

    # Determine the species name and protein name based on the file name
    if [[ $base_name == can_p450* ]]; then
        species_name="canadensis"
        protein_name="p450"
    elif [[ $base_name == chi_terpene* ]]; then
        species_name="chinensis"
        protein_name="terpene"
    elif [[ $base_name == can_terpene* ]]; then
        species_name="canadensis"
        protein_name="terpene"
    elif [[ $base_name == chi_p450* ]]; then
        species_name="chinensis"
        protein_name="p450"
    fi

    # Get the third column of the file (excluding the header)
    third_column=$(tail -n +2 "$file" | cut -f3)

    # Print the columns as separate lines with species name, protein name, and third column
    paste <(yes "$species_name" | head -n $(wc -l < "$file")) <(yes "$protein_name" | head -n $(wc -l < "$file")) <(echo "$third_column")

done >> output.txt

mv output.txt input_list.metadata
