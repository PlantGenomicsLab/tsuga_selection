#!/bin/bash
#SBATCH --job-name=CanTrimmomatic
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=200G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load Trimmomatic/0.39

readpairlist=/core/labs/Wegrzyn/karl/tsuga_sel/data/chinensis/assembly/01_Raw_Reads/filelist
for f in `cat $readpairlist`
do
java -jar $Trimmomatic PE -threads 4 \
        ../01_Raw_Reads/${f}_1.fastq.gz \
        ../01_Raw_Reads/${f}_2.fastq.gz \
        trim_${f}_1.fastq.gz singles_trim_${f}_1.fastq.gz \
        trim_${f}_2.fastq.gz singles_trim_${f}_2.fastq.gz \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        SLIDINGWINDOW:4:25 MINLEN:45

done
date
