#!/bin/bash

# Check if the correct number of arguments is provided
if [ $# -ne 1 ]; then
  echo "Usage: $0 <path/to/input_fasta.fa>"
  exit 1
fi

# Path to the original FASTA file
FASTA_FILE="$1"

# Check if the provided FASTA file exists
if [ ! -f "$FASTA_FILE" ]; then
  echo "Error: FASTA file $FASTA_FILE not found."
  exit 1
fi

# Loop through the files with the "_locus.txt" suffix in the current directory
for HEADER_FILE in *_locus.txt; do
  # Extract the base name of the header file to use as the output FASTA file name
  BASENAME=$(basename "$HEADER_FILE" _locus.txt)
  OUTPUT_FASTA="${BASENAME}.fasta"
  
  # Use seqkit to extract sequences matching the headers in the current header file
  seqkit grep -r -f "$HEADER_FILE" "$FASTA_FILE" > "$OUTPUT_FASTA"
  
  echo "Extracted sequences for $BASENAME into $OUTPUT_FASTA"
done

echo "Separation complete."
