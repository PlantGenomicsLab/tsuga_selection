#!/bin/bash
#SBATCH --job-name=build_tsuga_sel
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general 
#SBATCH --qos=general
#SBATCH --mem=1G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Creates the folder structure defined in folder structure section below
function createFolderStructure() {
     depth="1"
     while (( "$#" )); do
         while (( $1 != $depth )); do
             cd ..
             (( depth-- ))
         done
         shift
         mkdir "$1"
         cd "$1"
         (( depth++ ))
         shift
         shift
         out=""
         while [[ "$1" != "-" ]]; do
             out=$out" ""$1"
             shift
         done
         shift
         echo "$out" > README.md
     done
     while (( 1 != $depth )); do
         cd ..
         (( depth-- ))
     done
}

# Folder Structure Section and readme files.
read -r -d '' FOLDERSTRUCTURE << EOM
1 tsuga_sel - Head directory. Replace this read me with the gitlab document -  
     2 data - Directory for raw reads, the assembled transcriptomes, and other metadata -
          3 canadensis - Tsuga canadensis raw reads, assembly, and final transcriptome products -
                4 assembly - canadensis assembly - 
                    5 01_raw_reads - Reads from the Project #3930, SRA, and Vidya -
                    5 02_trimmed_reads - Reads trimmed by fastp -
                    5 03_ORP - Run ORP in here -
          3 chinensis - Tsuga chinensis raw reads, assembly, and final transcriptome products -
                4 assembly - chinensis assembly -
                    5 01_raw_reads - Reads from the Project #3930, SRA, and Vidya -
                    5 02_trimmed_reads - Reads trimmed by fastp -
                    5 02_ORP - Run ORP in here -
     2 methods - Dir for methods -
          3 scripts - Scripts for the analysis -
          3 lit - Store literature here -
     2 results - Results of tests and analyses - 
EOM

createFolderStructure $FOLDERSTRUCTURE


