#!/bin/bash
#SBATCH --job-name=CanEnTAP
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16 
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

#module load EnTAP/0.10.8
#module load diamond/2.0.12
# Current modules may be the issue,...
# Stick with the beta version & tutorial
module load EnTAP/0.9.0-beta
module load diamond/0.9.36

# not sure if these are needed ..V..
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0
module load eggnog-mapper/0.99.1

# Set paths
protein=/core/labs/Wegrzyn/karl/tsuga_sel/data/canadensis/assembly/06_Evaluate/centroids.pep

# run EnTAP, flagging bacterial and fungal hits as possible contaminants and favoring hits to Tsuga
EnTAP --runP \
-i $protein \
-d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.202.dmnd \
-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
-d /isg/shared/databases/Diamond/ntnr/nr_protein.202.dmnd \
--ontology 0 \
--threads 16 \
-c bacteria \
-c fungi \
--taxon Tsuga 

date 


