#!/bin/bash
#SBATCH --job-name=raw_data_symlinks
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=128M
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

fpath1="/archive/labs/wegrzyn/transcriptomes/conifers/hemlock/kfetter/tsuga_canadensis/"
fpath2="/archive/labs/wegrzyn/transcriptomes/conifers/hemlock/kfetter/tsuga_chinensis/"

cd /core/labs/Wegrzyn/karl/tsuga_sel/data/canadensis/assembly/01_Raw_Reads

for f in ${fpath1}*; do
        echo $f
        echo `basename ${f}`
        ln -s ${f} `basename ${f}`
done

cd /core/labs/Wegrzyn/karl/tsuga_sel/data/chinensis/assembly/01_Raw_Reads

for f in ${fpath2}*; do
        echo $f
        echo `basename ${f}`
        ln -s ${f} `basename ${f}`
done


