#!/bin/bash
#SBATCH --job-name=Grep2Pop
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

workdir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/

# The file containing the one-to-one TPS orthologs
tps_orthologs_1to1=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/one_to_one_tps_orthologs_tcan_tchi

# Take the locus file and use it with the cluster table to get a list of genes from the populaiton sets
## Specify the centroid table of parent-child relationships (uctable files)
uctable_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/05_Clustering/clusters.uc
uctable_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/05_Clustering/clusters.uc

# Reading the file line by line
while IFS=$'\t' read -r _ orthogroup_name can_locus chi_locus; do
    # Grep the content and save to a file named by orthogroup for canadensis
    grep "$can_locus" "$uctable_can" > "${orthogroup_name}_can.uc"
    
    # Grep the content and save to a file named by orthogroup for chinensis
    grep "$chi_locus" "$uctable_chi" > "${orthogroup_name}_chi.uc"
done < "$tps_orthologs_1to1"


# Remove the centroid records (removes duplicate information)
sed -i 's/^C.*$//g' *.uc
 
# Delete empty lines created after the sed
sed -i '/^$/d' *.uc 
 
# Path to the other script
scripts="/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/complete_cds/scripts"
 
# Loop through each file that matches the pattern *_cluster.uc
for OG_file in *.uc; do
     # Run the generate_grep_files.sh script with the OG_file as argument
     bash "$scripts/generate_grep_files.sh" "$OG_file"
 done
  
## Using seqkit, extract the desired protein sequences
# Protein files to grep from
complete_cds_pep_can=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep
complete_cds_pep_chi=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep

# Function to grep for genes
# This will create empty files for seqkit grep combinations that don't exist. Don't worry about it for now.
grep_genes() {
    local species_file="$1"
    local species_suffix="$2"

    for grep_file in *_grep-locus.txt; do
        output_file="${grep_file%_grep-locus.txt}_tps_${species_suffix}.pep.fa"
        seqkit grep -r -f "$grep_file" "$species_file" > "$output_file"
    done
}

# grep for canadensis
grep_genes "$complete_cds_pep_can" "can"

# grep for chinensis
grep_genes "$complete_cds_pep_chi" "chi"

## Add TCan for canadensis (to help tracking)
sed -i 's/^>/>Tcan_/' *can.pep.fa
sed -i 's/^>/>Tchi_/' *chi.pep.fa

# Tidy up any empty pep files
for pep_file in *.pep.fa; do
    if [[ ! -s "$pep_file" ]]; then
        rm "$pep_file"
    fi
done
