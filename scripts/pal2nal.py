#!/usr/bin/env python

import os
import subprocess
from Bio import SeqIO

pep_dir = "/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_mafft_aln"
cds_dir = "/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_cds_set"
output_dir = "/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_pal2nal"

os.makedirs(output_dir, exist_ok=True)

for pep_file in os.listdir(pep_dir):
    if pep_file.endswith(".pep.aln"):
        base_name = pep_file[:-9]
        cds_file = os.path.join(cds_dir, f"{base_name}.cds.fa")
        reordered_cds_file = os.path.join(cds_dir, f"{base_name}.reordered.cds.fa")
        output_file = os.path.join(output_dir, f"{base_name}.aligned.cds.fa")

        if os.path.isfile(cds_file):
            # Extract IDs from pep file
            pep_ids = [rec.id for rec in SeqIO.parse(os.path.join(pep_dir, pep_file), "fasta")]

            # Reorder CDS based on the order of PEP IDs
            cds_records = SeqIO.to_dict(SeqIO.parse(cds_file, "fasta"))
            reordered_cds = [cds_records[id] for id in pep_ids]

            SeqIO.write(reordered_cds, reordered_cds_file, "fasta")

            # Run pal2nal
            cmd = f"pal2nal.pl {os.path.join(pep_dir, pep_file)} {reordered_cds_file} -nomismatch -output fasta > {output_file}"
            subprocess.run(cmd, shell=True)

            print(f"Processed {pep_file} and {cds_file}")
        else:
            print(f"CDS file not found for {pep_file}")
