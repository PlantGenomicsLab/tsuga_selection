#!/bin/bash
#SBATCH --job-name=subsetCDS
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10M
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=first.last@uconn.edu

module load seqtk/1.3

cds=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds
keep_list=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/08_EnTAP/filter300bp/entap_outfiles/final_results/final_annotations_no_contam.transcript_ids

seqtk subseq $cds $keep_list > canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds