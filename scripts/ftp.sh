#!/bin/bash
#SBATCH --job-name=ftpSRA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 5
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
##SBATCH --mail-type=ALL
##SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

ssh kfetter@transfer.cam.uchc.edu

cd /archive/labs/wegrzyn/transcriptomes/conifers/hemlock/kfetter/tsuga_chinensis

curl -T sra_upload.tar.gz --user subftp:1CVwWYg7 ftp://ftp-private.ncbi.nlm.nih.gov/uploads/karl.fetter_gmail.com_1631vtKH/tsuga_sra_curl
  
