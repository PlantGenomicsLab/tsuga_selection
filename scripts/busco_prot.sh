#!/bin/bash
#SBATCH --job-name=CanBUSprot
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --qos=general
#SBATCH --partition=xeon
#SBATCH --mail-type=END
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH --mem=10G
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load seqtk/1.3 
module load busco/5.0.0

# Set paths of centroids
CENTROIDS=/core/labs/Wegrzyn/karl/tsuga_sel/data/canadensis/assembly/05_Clustering/centroids.fasta
PEPTIDES=/core/labs/Wegrzyn/karl/tsuga_sel/data/canadensis/assembly/04_Coding_Regions/trinity_combine.fasta.transdecoder.pep

# Use a perl grep to pull out the transcript names without the '>'
grep -oP "(?<=>).*" $CENTROIDS > transcript_ids

# Subset the protein sequences from the transcript id's in the names.txt file 
seqtk subseq $PEPTIDES transcript_ids > centroids.pep

# Perform busco protein run
odb=/isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10
busco -i centroids.pep -l $odb -o busco_protein -m prot --offline
