#!/bin/bash
#SBATCH --job-name=pull_seqs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# Make the input names
can_input=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/09_Final_Files/canadensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
chi_input=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/09_Final_Files/chinensis_reference.transdecoder.vserach.EnTAP_clean_annotated.cds
terp_list=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/compare/terpene_list.txt
p450_list=/core/labs/Wegrzyn/karl/tsuga_sel/protein_search/compare/p450_list.txt

# Create fasta for each species x protein
seqkit grep -f $terp_list $can_input > terpenoid_can.fa
seqkit grep -f $p450_list $can_input > p450_can.fa
seqkit grep -f $terp_list $chi_input > terpenoid_chi.fa
seqkit grep -f $p450_list $chi_input > p450_chi.fa

