#!/bin/bash
#SBATCH --job-name=Can300bpFilter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load seqtk

# Filter the clustered centroids for 300 bp sequences (remove them)
seqtk seq -L 300 ../05_Clustering/centroids.fasta > centroids_300bpFilter.fasta

# Pull out the names of the transcripts and send to file
grep ">" centroids_300bpFilter.fasta | sed 's/^>//g' > filtered_centroids.txt

# Subset the protein file that contains all of the sequnces for only the ones that are now centroids and also filtered
seqtk subseq ../04_Coding_Regions/trinity_combine.fasta.transdecoder.pep filtered_centroids.txt > centroids_300bpFilter.pep
