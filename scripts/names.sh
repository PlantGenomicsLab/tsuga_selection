#!/bin/bash
#SBATCH --job-name=CanNames
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=120G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=[0-19]
##SBATCH --mail-type=ALL
##SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

hostname
date

# a bash array containing the sample IDs
LIST=($(echo ACN_01 ACN_02 JSP_01 JSP_02 JSP_03 SRR12377238 SRR12377239 SRR12377240 SRR12377241 SRR6134124 SRR6134132 SRR6134201 SRR6134204 SRR6134205 TSP_01 TSP_02 TSP_03 Tca2187_98Aa Tca227_85Ab Tca263_47Aa))

# get one sample ID using the slurm array task ID
SAM=${LIST[$SLURM_ARRAY_TASK_ID]}

sed "s/>/>${SAM}_/g" ../03_Assembly/repeat350/trinity_${SAM}.Trinity.fasta > ../03_Assembly/repeat350/trinity_prefix_${SAM}.Trinity.fasta

date 
