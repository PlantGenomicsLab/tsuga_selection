#!/bin/bash

# Check if the pop_set file is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path_to_pop_set_file>"
    exit 1
fi

# Set the input file
pop_set_file="$1"

# Ensure the file exists
if [ ! -f "$pop_set_file" ]; then
    echo "Error: File '$pop_set_file' not found."
    exit 1
fi

# Initialize centroid variable
current_centroid=""

# Read the file line by line
while IFS=$'\t' read -r -a columns; do
    # Check the first column
    prefix="${columns[0]}"
    if [[ "$prefix" == "S" ]]; then
        current_centroid="${columns[8]}"
        echo "${current_centroid}" > "${current_centroid}.txt"
    elif [[ "$prefix" == "H" && -n "$current_centroid" ]]; then
        target="${columns[8]}"
        echo "${target}" >> "${current_centroid}.txt"
    fi
done < "$pop_set_file"

