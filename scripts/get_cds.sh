#!/bin/bash
#SBATCH --job-name=GetCDS
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

work_dir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_cds_set
head_dir=/core/labs/Wegrzyn/karl/tsuga_sel/multiple_seq_aln/hmmr_result/geneious_mafft_aln
can_cds=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds
chi_cds=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/chinensis/04_Coding_Regions/trinity_combine.fasta.transdecoder.cds

cd $head_dir

for grep_file in *headers; do
    species=$(echo $grep_file | grep -o "_can\." || echo "_chi.")
    if [ "$species" == "_can." ]; then
        cds=$can_cds
    else
        cds=$chi_cds
    fi
    out="${grep_file%%.pep.aln.headers}.cds.fa"
    seqkit grep -r -f $grep_file $cds > $work_dir/$out
done
