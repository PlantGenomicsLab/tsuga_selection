#!/bin/bash
#SBATCH --job-name=CHI_qc_raw
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=2G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

#################################################################
# FASTQC of raw reads 
#################################################################
# create an output directory to hold fastqc output
DIR="raw"
mkdir -p ${DIR}_fastqc

module load fastqc/0.11.7

SAM=ACH_01
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_02
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_03
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_04
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_05
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_06
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_12
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=ACH_13
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377231
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377232
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377233
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377234
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377235
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377237
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377248
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377259
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377268
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377269
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377270
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=SRR12377271
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz
SAM=Tch2007_Ba
fastqc --threads 4 --outdir ./${DIR}_fastqc/ ../01_Raw_Reads/${SAM}_1.fastq.gz ../01_Raw_Reads/${SAM}_2.fastq.gz

#################################################################
# MULTIQC of raw reads 
#################################################################
module load MultiQC/1.9

mkdir -p ${DIR}_multiqc
multiqc --outdir ${DIR}_multiqc ./${DIR}_fastqc/
