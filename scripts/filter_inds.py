import sys
from Bio import SeqIO

def remove_individuals(input_file, individuals_to_remove):
    for record in SeqIO.parse(input_file, 'fasta'):
        if record.id not in individuals_to_remove:
            SeqIO.write(record, sys.stdout, 'fasta')

def main():
    if len(sys.argv) != 3:
        print("Usage: python filter_inds.py /path/to/fasta.pep /path/to/individuals.txt", file=sys.stderr)
        sys.exit(1)

    input_file = sys.argv[1]
    individuals_file = sys.argv[2]

    with open(individuals_file, 'r') as file:
        individuals_to_remove = [line.strip() for line in file.readlines()]

    remove_individuals(input_file, individuals_to_remove)

if __name__ == "__main__":
    main()


