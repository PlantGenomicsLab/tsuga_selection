#!/bin/bash
#SBATCH --job-name=ChiRnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16 
#SBATCH --mem=200G
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --array=[0-20]
##SBATCH --mail-type=ALL
##SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

hostname
date

##################################################
## 		rnaQUAST			##
##################################################

module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1

# a bash array containing the assembled transcripts for each sample
LIST=($(echo trinity_prefix_ACH_01.Trinity.fasta trinity_prefix_ACH_02.Trinity.fasta trinity_prefix_ACH_03.Trinity.fasta trinity_prefix_ACH_04.Trinity.fasta trinity_prefix_ACH_05.Trinity.fasta trinity_prefix_ACH_06.Trinity.fasta trinity_prefix_ACH_12.Trinity.fasta trinity_prefix_ACH_13.Trinity.fasta trinity_prefix_SRR12377231.Trinity.fasta trinity_prefix_SRR12377232.Trinity.fasta trinity_prefix_SRR12377233.Trinity.fasta trinity_prefix_SRR12377234.Trinity.fasta trinity_prefix_SRR12377235.Trinity.fasta trinity_prefix_SRR12377237.Trinity.fasta trinity_prefix_SRR12377248.Trinity.fasta trinity_prefix_SRR12377259.Trinity.fasta trinity_prefix_SRR12377268.Trinity.fasta trinity_prefix_SRR12377269.Trinity.fasta trinity_prefix_SRR12377270.Trinity.fasta trinity_prefix_SRR12377271.Trinity.fasta trinity_prefix_Tch2007_Ba.Trinity.fasta))  

# get one sample ID using the slurm array task ID
assembly=${LIST[$SLURM_ARRAY_TASK_ID]}

# Run rnaQUAST with GeneMarkS-T
rnaQUAST.py --transcripts ../${assembly} \
	--gene_mark \
	--threads 16 \
	--output_dir results_${assembly}

date 


