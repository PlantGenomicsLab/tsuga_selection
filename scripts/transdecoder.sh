#!/bin/bash
#SBATCH --job-name=CanTransdecoder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

# concatenate the assemblies into a single file
#cat ../03_Assembly/assembly_min_contig_length_350/trinity_prefix_* > ../03_Assembly/assembly_min_contig_length_350/trinity_combine.fasta


##################################################
## Determine ORF using Transdecoder		##
##################################################

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../03_Assembly/assembly_min_contig_length_350/trinity_combine.fasta

hmmscan --cpu 16 \
       --domtblout pfam.domtblout \
       /isg/shared/databases/Pfam/Pfam-A.hmm \
       trinity_combine.fasta.transdecoder_dir/longest_orfs.pep 

TransDecoder.Predict -t ../03_Assembly/assembly_min_contig_length_350/trinity_combine.fasta \
	--retain_pfam_hits pfam.domtblout \
	--cpu 16 
