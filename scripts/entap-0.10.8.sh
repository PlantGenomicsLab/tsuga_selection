#!/bin/bash
#SBATCH --job-name=CanEnTAP
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32 
#SBATCH --mem=150G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=kcf@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/2.0.6
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0
module load eggnog-mapper/0.99.1

# Set paths
protein=/core/labs/Wegrzyn/karl/tsuga_sel/transcriptome/canadensis/06_Filter/centroids_300bpFilter.pep

# Run EnTAP
EnTAP --runP \
    --ini entap_config.ini \
    -t 32 \
    -i $protein \
    -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd \
    -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
    -d /isg/shared/databases/Diamond/ntnr/nr_protein.202.dmnd
     